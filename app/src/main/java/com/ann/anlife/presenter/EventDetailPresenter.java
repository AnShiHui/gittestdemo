package com.ann.anlife.presenter;

import com.alibaba.fastjson.JSON;
import com.ann.anlife.base.BaseImpl;
import com.ann.anlife.http.HttpImpl;
import com.ann.anlife.http.HttpUrls;
import com.ann.anlife.interfaces.OnHttpCallback;
import com.ann.anlife.model.EventDetailInfo;
import com.ann.anlife.viewmodel.BaseEventView;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;

/**
 * author Ann
 * description
 * createtime 2019/9/21
 */
public class EventDetailPresenter extends BaseImpl {

    private BaseEventView eventView;
    private CompositeDisposable disposable = getCompositeDisposabe();
    private HttpImpl http;

    public EventDetailPresenter(BaseEventView eventView) {
        if (eventView == null){
            throw new NullPointerException("BaseEventView must not be null");
        }
        this.eventView = eventView;
        if (http == null){
            http = new HttpImpl(disposable);
        }
    }

    public void getEventDetail(int eid){
        Map<String,Object> map = new HashMap<>();
        map.put("e_id",eid);
        map.put("key","e85e9ede682e5e37748aa3ca18666a51");
        http.startRequest(HttpUrls.mHistoryDetailUrl, map, new OnHttpCallback() {
            @Override
            public void onSuccess(String result) {
                eventView.onEventDetailLoad(JSON.parseObject(result,EventDetailInfo.class));
            }

            @Override
            public void onFail(String reason) {
                eventView.onEventLoadFail(reason);
            }

            @Override
            public void onNetError(String error) {
                eventView.onNetError();
            }
        });
    }

    @Override
    public void onRelease() {
        if (disposable != null){
            disposable.clear();
        }
    }
}
