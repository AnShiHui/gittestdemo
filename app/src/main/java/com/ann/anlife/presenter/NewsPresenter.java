package com.ann.anlife.presenter;

import com.alibaba.fastjson.JSON;
import com.ann.anlife.base.BaseImpl;
import com.ann.anlife.http.HttpImpl;
import com.ann.anlife.http.HttpUrls;
import com.ann.anlife.interfaces.OnHttpCallback;
import com.ann.anlife.model.NewsInfo;
import com.ann.anlife.utils.LogUtil;
import com.ann.anlife.viewmodel.BaseNewsView;

import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;

/**
 * author Ann
 * description
 * createtime 2019/9/17
 */
public class NewsPresenter extends BaseImpl {

    private final BaseNewsView baseNewsView;
    private final CompositeDisposable disposable = getCompositeDisposabe();
    private HttpImpl mHttp;

    public NewsPresenter(BaseNewsView baseNewsView) {
        if (baseNewsView == null){
            throw new NullPointerException("BaseNewsView must not be null");
        }
        this.baseNewsView = baseNewsView;
        if (mHttp == null){
            mHttp = new HttpImpl(disposable);
        }
    }

    //获取新闻信息
    public void getNewsInfo(String type){
        Map<String,Object> map = getHashMap();
        map.put("type",type);
        map.put("key","503fb6dc269b97dcb7977c7b882b43e3");
        mHttp.startRequest(HttpUrls.mToutiaoUrl, map, new OnHttpCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtil.d("on success "+result);
                baseNewsView.onLoadNews(JSON.parseObject(result,NewsInfo.class));
            }

            @Override
            public void onFail(String reason) {
                LogUtil.d("on fail "+reason);
                baseNewsView.onLoadFail(reason);
            }

            @Override
            public void onNetError(String error) {
                LogUtil.d("on error "+error);
                baseNewsView.onNetError();
            }
        });
    }

    public void onRelease(){
        if (!disposable.isDisposed()){
            disposable.dispose();
        }
    }
}
