package com.ann.anlife.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ann.anlife.R;
import com.ann.anlife.interfaces.OnRecyclerViewItemClickCallback;
import com.ann.anlife.model.WeChatInfo;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * author Ann
 * description
 * createtime 2019/9/20
 */
public class WechatRecyclerViewAdapter extends RecyclerView.Adapter implements View.OnClickListener {

    private List<WeChatInfo.ResultBean.ListBean> listBeans;
    private Context context;
    private OnRecyclerViewItemClickCallback itemClickCallback;
    private static final int TYPE_NORMAL = 0;
    private static final int TYPE_FOOT = 1;
    private boolean isSuccess;

    public WechatRecyclerViewAdapter(List<WeChatInfo.ResultBean.ListBean> listBeans, Context context) {
        this.listBeans = listBeans;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_NORMAL){
            View view = LayoutInflater.from(context).inflate(R.layout.layout_wechat_adapter,parent,false);
            return new WechatHolder(view);
        }else if (viewType == TYPE_FOOT){
            View footerView = LayoutInflater.from(context).inflate(R.layout.layout_newsitem_foot,parent,false);
            return new FooterHolder(footerView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (listBeans != null && position < listBeans.size()){
            if (holder instanceof WechatHolder){
                WechatHolder wechatHolder = (WechatHolder) holder;
                wechatHolder.tv_info.setText(listBeans.get(position).getSource());
                wechatHolder.tv_title.setText(listBeans.get(position).getTitle());
                wechatHolder.itemView.setTag(position);
                wechatHolder.itemView.setOnClickListener(this);
            }else if (holder instanceof FooterHolder){
                FooterHolder footerHolder = (FooterHolder) holder;
                if (isSuccess){
                    footerHolder.itemView.setVisibility(View.GONE);
                }else {
                    footerHolder.contentLoadingProgressBar.setVisibility(View.GONE);
                    footerHolder.textView.setText("没有更多精选数据了");
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return listBeans == null || listBeans.size() == 0 ? 0 :listBeans.size() + 1;
    }

    public void setItemClickCallback(OnRecyclerViewItemClickCallback itemClickCallback) {
        this.itemClickCallback = itemClickCallback;
    }

    @Override
    public int getItemViewType(int position) {
        if (listBeans != null && listBeans.size() > 0 ){
            if (position >= listBeans.size()){
                return TYPE_FOOT;
            }else
                return TYPE_NORMAL;
        }
        return super.getItemViewType(position);
    }

    public void onFinished(boolean isSuccess){
        this.isSuccess = isSuccess;
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        int postion = (int) view.getTag();
        if(itemClickCallback != null){
            itemClickCallback.onItemClick(postion);
        }
    }

    static class WechatHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.wechat_adapter_info)
        AppCompatTextView tv_info;
        @BindView(R.id.wechat_adapter_title)
        AppCompatTextView tv_title;
        WechatHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
    static class FooterHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.footer_progress)
        ContentLoadingProgressBar contentLoadingProgressBar;
        @BindView(R.id.footer_title)
        AppCompatTextView textView;
        FooterHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
