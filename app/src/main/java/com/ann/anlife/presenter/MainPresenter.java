package com.ann.anlife.presenter;

import com.alibaba.fastjson.JSON;
import com.ann.anlife.base.BaseImpl;
import com.ann.anlife.http.HttpImpl;
import com.ann.anlife.http.HttpUrls;
import com.ann.anlife.interfaces.OnHttpCallback;
import com.ann.anlife.model.TodayInfo;
import com.ann.anlife.model.WeatherInfo;
import com.ann.anlife.viewmodel.BaseMainView;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;

/**
 * author Ann
 * description
 * createtime 2019/9/20
 */
public class MainPresenter extends BaseImpl {

    private BaseMainView mainView;
    private CompositeDisposable disposable = getCompositeDisposabe();
    private HttpImpl mHttp;

    public MainPresenter(BaseMainView mainView) {
        if (mainView == null){
            throw new NullPointerException("BaseMainView must not be null");
        }
        this.mainView = mainView;
        if (mHttp == null){
            mHttp = new HttpImpl(disposable);
        }
    }

    /*
     * 获取历史上的今天
     */
    public void getToadyInfo(){
        Map<String,Object> map = getHashMap();
        Calendar calendar = Calendar.getInstance();
        map.put("date",calendar.get(Calendar.MONTH)+1+"/"+calendar.get(Calendar.DAY_OF_MONTH));
        map.put("key","e85e9ede682e5e37748aa3ca18666a51");
        mHttp.startRequest(HttpUrls.mHistoryListUrl, map, new OnHttpCallback() {
            @Override
            public void onSuccess(String result) {
                mainView.onTodayInHistory(JSON.parseObject(result,TodayInfo.class));
            }

            @Override
            public void onFail(String reason) {
                mainView.onTodayLoadFail(reason);
            }

            @Override
            public void onNetError(String error) {
                mainView.onNetError();
            }
        });
    }

    /*获取天气信息*/
    public void getWeatherInfo(String city) {
        Map<String,Object> map = getHashMap();
        map.put("city",city);
        map.put("key","9078607f4a6be7a0386700c81373cd12");
        mHttp.startRequest(HttpUrls.mWeatherQueryUrl, map, new OnHttpCallback() {
            @Override
            public void onSuccess(String result) {
                mainView.onWeatherLoad(JSON.parseObject(result,WeatherInfo.class));
            }

            @Override
            public void onFail(String reason) {
                mainView.onWeatherFail(reason);
            }

            @Override
            public void onNetError(String error) {
                mainView.onNetError();
            }
        });
    }

    @Override
    public void onRelease() {
        if (disposable != null){
            disposable.clear();
        }
    }
}
