package com.ann.anlife.presenter;

import com.alibaba.fastjson.JSON;
import com.ann.anlife.base.BaseImpl;
import com.ann.anlife.http.HttpImpl;
import com.ann.anlife.http.HttpUrls;
import com.ann.anlife.interfaces.OnHttpCallback;
import com.ann.anlife.model.WeChatInfo;
import com.ann.anlife.viewmodel.BaseWechatView;

import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;

/**
 * author Ann
 * description
 * createtime 2019/9/20
 */
public class WechatPresenter extends BaseImpl {

    private BaseWechatView wechatView;
    private CompositeDisposable disposable = getCompositeDisposabe();
    private HttpImpl mHttp;

    public WechatPresenter(BaseWechatView wechatView) {
        if (wechatView == null){
            throw new NullPointerException("BaseWechatView must not be null");
        }
        this.wechatView = wechatView;
        mHttp = new HttpImpl(disposable);
    }

    public void getWechatInfo(int page,int count){
        Map<String,Object> map = getHashMap();
        map.put("key","808b3c0a6694abadb6531ea89f8099d5");
        map.put("pno",page);
        map.put("ps",count);
        mHttp.startRequest(HttpUrls.mWechatUrl, map, new OnHttpCallback() {
            @Override
            public void onSuccess(String result) {
                wechatView.onWechatLoad(JSON.parseObject(result, WeChatInfo.class));
            }

            @Override
            public void onFail(String reason) {
                wechatView.onWechatLoadFail(reason);
            }

            @Override
            public void onNetError(String error) {
                wechatView.onNetError();
            }
        });
    }

    //加载更多
    public void getWechatMoreInfo(int page,int count){
        Map<String,Object> map = getHashMap();
        map.put("key","808b3c0a6694abadb6531ea89f8099d5");
        map.put("pno",page);
        map.put("ps",count);
        mHttp.startRequest(HttpUrls.mWechatUrl, map, new OnHttpCallback() {
            @Override
            public void onSuccess(String result) {
                wechatView.onWechatLoadMore(JSON.parseObject(result,WeChatInfo.class));
            }

            @Override
            public void onFail(String reason) {
                wechatView.onWechatLoadFail(reason);
            }

            @Override
            public void onNetError(String error) {
                wechatView.onNetError();
            }
        });
    }

    @Override
    public void onRelease() {
        if (disposable != null){
            disposable.clear();
        }
    }
}
