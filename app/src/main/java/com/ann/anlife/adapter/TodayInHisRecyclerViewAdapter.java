package com.ann.anlife.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ann.anlife.R;
import com.ann.anlife.interfaces.OnRecyclerViewItemClickCallback;
import com.ann.anlife.model.TodayInfo;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * author Ann
 * description
 * createtime 2019/9/21
 */
public class TodayInHisRecyclerViewAdapter extends RecyclerView.Adapter implements View.OnClickListener {

    private ArrayList<TodayInfo.ResultBean> list;
    private Context context;
    private OnRecyclerViewItemClickCallback clickCallback;

    public TodayInHisRecyclerViewAdapter(ArrayList<TodayInfo.ResultBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void setClickCallback(OnRecyclerViewItemClickCallback clickCallback) {
        this.clickCallback = clickCallback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_todaylist_adapter, parent, false);
        return new TodayHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(this);
        if (list.size() > 0 && position < list.size()){
            TodayHolder todayHolder = (TodayHolder) holder;
            todayHolder.todaylistAdapterTitle.setText(list.get(position).getTitle());
            todayHolder.todaylistAdapterInfo.setText(list.get(position).getDate());
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
        if (clickCallback != null){
            clickCallback.onItemClick(position);
        }
    }

    class TodayHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.todaylist_adapter_title)
        AppCompatTextView todaylistAdapterTitle;
        @BindView(R.id.todaylist_adapter_info)
        AppCompatTextView todaylistAdapterInfo;
        public TodayHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
