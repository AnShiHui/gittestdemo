package com.ann.anlife.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ann.anlife.R;
import com.ann.anlife.model.LotterHistoryInfo;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * author Ann
 * description
 * createtime 2019/11/24
 */
public class LotterHistoryRecyclerViewAdapter extends RecyclerView.Adapter<LotterHistoryRecyclerViewAdapter.LotterHistoryHolder> {

    private Unbinder unbinder;
    private Context context;
    private List<LotterHistoryInfo.ResultBean.LotteryResListBean> list;

    public LotterHistoryRecyclerViewAdapter(Context context, List<LotterHistoryInfo.ResultBean.LotteryResListBean> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public LotterHistoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_lotter_history_adapter, parent, false);
        return new LotterHistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LotterHistoryHolder holder, int position) {
        if (list.size() > 0 && list.size() > position) {
            holder.lotterHisNo.setText("开奖期号："+list.get(position).getLottery_no());
            holder.lotterHisDate.setText("开奖日期: " + list.get(position).getLottery_date());
            holder.lotterHisDeadline.setText("兑换截止日期：" + list.get(position).getLottery_exdate());
            holder.lotterHisMoney.setText("本期销售额：" + list.get(position).getLottery_sale_amount());
            holder.lotterHisNum.setText("开奖号码："+list.get(position).getLottery_res());
            holder.lotterHisPool.setText("奖池滚存："+list.get(position).getLottery_pool_amount());
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class LotterHistoryHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lotter_his_num)
        AppCompatTextView lotterHisNum;
        @BindView(R.id.lotter_his_no)
        AppCompatTextView lotterHisNo;
        @BindView(R.id.lotter_his_date)
        AppCompatTextView lotterHisDate;
        @BindView(R.id.lotter_his_deadline)
        AppCompatTextView lotterHisDeadline;
        @BindView(R.id.lotter_his_money)
        AppCompatTextView lotterHisMoney;
        @BindView(R.id.lotter_his_pool)
        AppCompatTextView lotterHisPool;

        LotterHistoryHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        unbinder.unbind();
    }
}
