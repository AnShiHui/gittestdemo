package com.ann.anlife.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;

import com.ann.anlife.R;
import com.ann.anlife.adapter.EventDetailGridViewAdapter;
import com.ann.anlife.base.BaseActivity;
import com.ann.anlife.model.EventDetailInfo;
import com.ann.anlife.presenter.EventDetailPresenter;
import com.ann.anlife.view.MyGridView;
import com.ann.anlife.viewmodel.BaseEventView;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryEventDetailActivity extends BaseActivity implements BaseEventView {

    @BindView(R.id.event_detail_toolbar)
    Toolbar eventDetailToolbar;
    @BindView(R.id.event_detail_content)
    AppCompatTextView eventDetailContent;
    @BindView(R.id.event_detail_gridview)
    MyGridView eventDetailGridview;
    private EventDetailPresenter detailPresenter;
    private EventDetailGridViewAdapter adapter;
    private List<EventDetailInfo.ResultBean.PicUrlBean> picUrlBeanList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_event_detail);
        ButterKnife.bind(this);
        StatusBarUtil.setLightMode(this);
        eventDetailToolbar.setTitle(getIntent().getStringExtra("title") == null ? "历史上的今天" : getIntent().getStringExtra("title"));
        setToolbar(eventDetailToolbar);
        initView();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        detailPresenter = new EventDetailPresenter(this);
        eventDetailContent.setMovementMethod(ScrollingMovementMethod.getInstance());
        adapter = new EventDetailGridViewAdapter(picUrlBeanList, HistoryEventDetailActivity.this);
        eventDetailGridview.setAdapter(adapter);
        eventDetailToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        detailPresenter.getEventDetail(Integer.parseInt(getIntent().getStringExtra("eid")));
    }

    @Override
    public void onEventDetailLoad(EventDetailInfo eventDetailInfo) {
        eventDetailContent.setText(eventDetailInfo.getResult().get(0).getContent());
        picUrlBeanList.addAll(eventDetailInfo.getResult().get(0).getPicUrl());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onEventLoadFail(String reason) {

    }

    @Override
    public void onNetError() {

    }

}
