package com.ann.anlife.activity;

import android.content.Intent;
import android.os.Bundle;

import com.ann.anlife.R;
import com.ann.anlife.adapter.TodayInHisRecyclerViewAdapter;
import com.ann.anlife.base.BaseActivity;
import com.ann.anlife.interfaces.OnRecyclerViewItemClickCallback;
import com.ann.anlife.model.TodayInfo;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryListActivity extends BaseActivity implements OnRecyclerViewItemClickCallback {

    @BindView(R.id.history_list_toolbar)
    Toolbar historyListToolbar;
    @BindView(R.id.history_list_recyclerview)
    RecyclerView historyListRecyclerview;
    private TodayInHisRecyclerViewAdapter adapter;
    private ArrayList<TodayInfo.ResultBean> resultBeanArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_list);
        ButterKnife.bind(this);
        StatusBarUtil.setLightMode(this);
        initRecyclerView();
    }

    private void initRecyclerView() {
        setToolbar(historyListToolbar);
        resultBeanArrayList = getIntent().getParcelableArrayListExtra("list");
        if (resultBeanArrayList == null){
            resultBeanArrayList = new ArrayList<>();
        }
        adapter = new TodayInHisRecyclerViewAdapter(resultBeanArrayList,this);
        adapter.setClickCallback(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        historyListRecyclerview.setLayoutManager(linearLayoutManager);
        historyListRecyclerview.setAdapter(adapter);
        historyListToolbar.setNavigationOnClickListener(view -> finish());
    }

    @Override
    public void onItemClick(int postion) {
        if (postion < resultBeanArrayList.size()){
            Intent intent = new Intent(HistoryListActivity.this,HistoryEventDetailActivity.class);
            intent.putExtra("title",resultBeanArrayList.get(postion).getTitle());
            intent.putExtra("eid",resultBeanArrayList.get(postion).getE_id());
            startActivity(intent);
        }
    }
}
