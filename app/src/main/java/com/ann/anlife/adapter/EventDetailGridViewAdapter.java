package com.ann.anlife.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ann.anlife.R;
import com.ann.anlife.model.EventDetailInfo;
import com.ann.anlife.view.CircleImageView;
import com.bumptech.glide.Glide;

import java.util.List;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * author Ann
 * description
 * createtime 2019/9/21
 */
public class EventDetailGridViewAdapter extends BaseAdapter {

    private List<EventDetailInfo.ResultBean.PicUrlBean> picUrlBeans;
    private Context context;
    private LayoutInflater layoutInflater;

    public EventDetailGridViewAdapter(List<EventDetailInfo.ResultBean.PicUrlBean> picUrlBeans, Context context) {
        this.picUrlBeans = picUrlBeans;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return picUrlBeans == null ? 0 : picUrlBeans.size();
    }

    @Override
    public Object getItem(int i) {
        return picUrlBeans.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        EventDetailHolder eventDetailHolder;
        if (view == null){
            view = layoutInflater.inflate(R.layout.layout_eventdetail_adapter,viewGroup,false);
            eventDetailHolder = new EventDetailHolder();
            eventDetailHolder.circleImageView = view.findViewById(R.id.event_adapter_iv);
            eventDetailHolder.tv_title = view.findViewById(R.id.event_adapter_title);
            view.setTag(eventDetailHolder);
        }else {
            eventDetailHolder = (EventDetailHolder) view.getTag();
        }
        if (picUrlBeans != null && i < picUrlBeans.size()){
            Glide.with(context).asBitmap().load(picUrlBeans.get(i).getUrl()).into(eventDetailHolder.circleImageView);
            eventDetailHolder.tv_title.setText(picUrlBeans.get(i).getPic_title());
            eventDetailHolder.tv_title.setVisibility(picUrlBeans.get(i).getPic_title().isEmpty() ? View.INVISIBLE : View.VISIBLE);
        }
        return view;
    }
    private class EventDetailHolder{
        private CircleImageView circleImageView;
        private AppCompatTextView tv_title;
    }
}
