package com.ann.anlife.viewmodel;

import com.ann.anlife.base.BaseView;
import com.ann.anlife.model.LotterHistoryInfo;

/**
 * author Ann
 * description
 * createtime 2019/11/24
 */
public interface BaseLotterHisView extends BaseView {

    void onHistoryLoad(LotterHistoryInfo historyInfo);

    void onHistoryFail(String reason);
}
