package com.ann.anlife.viewmodel;

import com.ann.anlife.base.BaseView;
import com.ann.anlife.model.NewsInfo;

/**
 * author Ann
 * description
 * createtime 2019/9/17
 */
public interface BaseNewsView extends BaseView {

    void onLoadNews(NewsInfo newsInfo);//获取新闻资讯

    void onLoadFail(String failMsg);//获取新闻资讯失败

    void onLoadEmpty();
}
