package com.ann.anlife.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.ann.anlife.R;
import com.ann.anlife.base.BaseActivity;
import com.ann.anlife.fragments.PageOneFragment;
import com.ann.anlife.fragments.PageThreeFragment;
import com.ann.anlife.fragments.PageTwoFragment;
import com.ann.anlife.interfaces.OnEachPermissionCallback;
import com.ann.anlife.model.TodayInfo;
import com.ann.anlife.model.WeatherInfo;
import com.ann.anlife.presenter.MainPresenter;
import com.ann.anlife.utils.LogUtil;
import com.ann.anlife.view.CircleImageView;
import com.ann.anlife.view.SwitcherView;
import com.ann.anlife.viewmodel.BaseMainView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.jaeger.library.StatusBarUtil;
import com.tbruyelle.rxpermissions2.Permission;
import com.tencent.bugly.beta.Beta;

import org.litepal.LitePal;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements BaseMainView,OnEachPermissionCallback, AMapLocationListener {

    @BindView(R.id.main_toolbar)
    Toolbar mainToolbar;
    @BindView(R.id.main_weather_icon)
    AppCompatImageView mainWeatherIcon;
    @BindView(R.id.main_weather_info)
    AppCompatTextView mainWeatherInfo;
    @BindView(R.id.main_nav)
    NavigationView mainNav;
    @BindView(R.id.main_drawerlayout)
    DrawerLayout mainDrawerlayout;
    @BindView(R.id.main_nav_container)
    FrameLayout mainNavContainer;
    @BindView(R.id.main_nav_bottom)
    BottomNavigationView mainNavBottom;
    @BindView(R.id.main_switchview)
    SwitcherView mainSwitchview;
    private CircleImageView circleImageView;
    private AppCompatTextView textViewName;
    private Fragment currentFragment = new Fragment();
    private FragmentManager manager;
    private PageOneFragment pageOneFragment;
    private PageTwoFragment pageTwoFragment;
    private PageThreeFragment pageThreeFragment;
    private String fragmentTag;
    private MainPresenter mainPresenter;
    private ArrayList<TodayInfo.ResultBean> todayList = new ArrayList<>();
    //声明mlocationClient对象
    public AMapLocationClient mlocationClient;
    //声明mLocationOption对象
    public AMapLocationClientOption mLocationOption = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            savedInstanceState.remove("androidx.fragment.app:frgaments");
        }
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        manager = getSupportFragmentManager();
        initBottomBar();
        initNav();
        StatusBarUtil.setLightMode(this);
        initMapSetting();
    }

    private void initMapSetting() {
        mlocationClient = new AMapLocationClient(this);
        mLocationOption = new AMapLocationClientOption();
        mlocationClient.setLocationListener(this);
        mLocationOption.setInterval(6000);
        mLocationOption.setNeedAddress(true);
        mLocationOption.setMockEnable(true);
        mLocationOption.setLocationCacheEnable(true);
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        mlocationClient.setLocationOption(mLocationOption);
    }

    /*导航栏*/
    private void initBottomBar() {
        mainNavBottom.setSelectedItemId(R.id.main_bottom_one);
        mainNavBottom.setOnNavigationItemSelectedListener(item -> {
            int itemId = item.getItemId();
            switch (itemId) {
                case R.id.main_bottom_one:
                    if (pageOneFragment == null) {
                        pageOneFragment = new PageOneFragment();
                    }
                    showFragment(pageOneFragment, "pageOneFragment");
                    return true;
                case R.id.main_bottom_two:
                    if (pageTwoFragment == null) {
                        pageTwoFragment = new PageTwoFragment();
                    }
                    showFragment(pageTwoFragment, "pageTwoFragment");
                    return true;
                case R.id.main_bottom_three:
                    if (pageThreeFragment == null) {
                        pageThreeFragment = new PageThreeFragment();
                    }
                    showFragment(pageThreeFragment, "pageThreeFragment");
                    return true;
            }
            return false;
        });
        setDefaultFragment();
    }

    /**
     * 设置第一个显示的fragment
     */
    private void setDefaultFragment() {
        if (pageOneFragment == null) {
            pageOneFragment = new PageOneFragment();
        }
        showFragment(pageOneFragment, "pageOneFragment");
    }

    //设置基本控件
    private void initNav() {
        View header = mainNav.inflateHeaderView(R.layout.layout_nav_header);
        setSupportActionBar(mainToolbar);
        mainNav.setItemIconTintList(null);
        circleImageView = header.findViewById(R.id.nav_header_icon);
        textViewName = header.findViewById(R.id.nav_header_name);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(MainActivity.this, mainDrawerlayout, mainToolbar, R.string.nav_close, R.string.nav_open) {
                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                }

                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                }

                @Override
                public void onDrawerSlide(View drawerView, float slideOffset) {
                    super.onDrawerSlide(drawerView, slideOffset);
                }
            };
            drawerToggle.syncState();
            mainDrawerlayout.addDrawerListener(drawerToggle);
        }
        mainPresenter = new MainPresenter(this);
        mainPresenter.getToadyInfo();
        mainSwitchview.setOnClickListener(view -> {
            if (todayList != null){
                Intent intent = new Intent();
                intent.setClass(MainActivity.this,HistoryListActivity.class);
                intent.putExtra("list",todayList);
                startActivity(intent);
            }
        });
        mainNav.setNavigationItemSelectedListener(item -> {
            switch (item.getItemId()){
                case R.id.main_nav_update:
                    Beta.checkUpgrade();
                    break;
                case R.id.main_nav_about:
                    break;
                case R.id.main_nav_setting:
                    startActivity(new Intent(MainActivity.this,SettingActivity.class));
                    break;
                case R.id.main_nav_suggestion:
                    break;
            }
            return false;
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*
        * 6.0版本权限申请
        * */
        if (Build.VERSION.SDK_INT >= 23) {
            String[] permissions = {
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };
           checkPermissionForEach(this,permissions);
        }
        if (mlocationClient != null){
            mlocationClient.startLocation();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mlocationClient != null){
            mlocationClient.stopLocation();
        }
    }

    //展示Fragment
    private void showFragment(Fragment fragment, String tags) {
        fragmentTag = tags;
        if (fragment != null && currentFragment != fragment) {//  判断传入的fragment是不是当前的currentFragmentgit
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.hide(currentFragment);//  不是则隐藏
            currentFragment = fragment;  //  然后将传入的fragment赋值给currentFragment
            if (!fragment.isAdded()) { //  判断传入的fragment是否已经被add()过
                transaction.add(R.id.main_nav_container, fragment, tags).show(fragment).commitAllowingStateLoss();
            } else {
                transaction.show(fragment).commitAllowingStateLoss();
            }
        }
    }

    /*获取历史上的今天事件列表*/
    @Override
    public void onTodayInHistory(TodayInfo todayInfo) {
        todayList.addAll(todayInfo.getResult());
        mainSwitchview.setResource(todayList);
        mainSwitchview.startRolling();
    }

    @Override
    protected void onStart() {
        if (!mainSwitchview.isRolling()){
            mainSwitchview.startRolling();
        }
        super.onStart();
    }

    @Override
    protected void onPause() {
        if (mainSwitchview.isRolling()){
            mainSwitchview.stopRolling();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (mainSwitchview != null){
            mainSwitchview.stopRolling();
            mainSwitchview.destroySwitcher();
            mainSwitchview = null;
        }
        if (mainPresenter != null){
            mainPresenter = null;
        }
        super.onDestroy();
    }

    @Override
    public void onTodayLoadFail(String reason) {

    }

    @Override
    public void onWeatherLoad(WeatherInfo weatherInfo) {
        if (weatherInfo != null){
            WeatherInfo.ResultBean result = weatherInfo.getResult();
            WeatherInfo.ResultBean.RealtimeBean realtime = result.getRealtime();
            mainWeatherInfo.setText(result.getCity()+"  ."+realtime.getInfo()+"\n实时气温："+realtime.getTemperature()+"°C");
            if (weatherInfo.isSaved()){
                weatherInfo.updateAll();
            }
        }
    }

    @Override
    public void onWeatherFail(String reason) {
        LogUtil.d("on Weather fail "+reason);
        WeatherInfo weatherInfo = LitePal.findLast(WeatherInfo.class);
        if (weatherInfo != null && weatherInfo.getResult() != null && weatherInfo.getResult().getRealtime() != null) {
            mainWeatherInfo.setText(weatherInfo.getResult().getCity() + "  ." + weatherInfo.getResult().getRealtime().getInfo() + "\n实时气温：" + weatherInfo.getResult().getRealtime().getTemperature() + "°C");
        }

    }

    @Override
    public void onNetError() {

    }

    @Override
    public void onPermissionNext(Permission permission) {
        LogUtil.d("permission info "+permission.name+" boolean "+permission.granted);
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation != null && aMapLocation.getErrorCode() == 0){
            String district = aMapLocation.getDistrict().replace("区","").trim();
            LogUtil.d("district "+district);
            mainPresenter.getWeatherInfo(district);
            mlocationClient.stopLocation();
        }else {
            if (aMapLocation != null) {
                LogUtil.d("errorCode "+aMapLocation.getErrorCode()+" errorInfo "+aMapLocation.getErrorInfo());
            }
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
