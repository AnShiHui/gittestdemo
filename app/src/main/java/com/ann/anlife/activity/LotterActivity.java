package com.ann.anlife.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ann.anlife.R;
import com.ann.anlife.adapter.LotterRecyclerViewAdapter;
import com.ann.anlife.base.BaseActivity;
import com.ann.anlife.base.BaseContants;
import com.ann.anlife.interfaces.OnRecyclerViewItemClickCallback;
import com.ann.anlife.model.LotterSortInfo;
import com.ann.anlife.presenter.LotterPresenter;
import com.ann.anlife.view.ProgressConstraintLayout;
import com.ann.anlife.viewmodel.BaseLotterSortView;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LotterActivity extends BaseActivity implements BaseLotterSortView, OnRecyclerViewItemClickCallback {

    @BindView(R.id.lotter_toolbar)
    Toolbar lotterToolbar;
    @BindView(R.id.lotter_recyclerView)
    RecyclerView lotterRecyclerView;
    @BindView(R.id.lotter_state)
    ProgressConstraintLayout lotterState;
    private LotterRecyclerViewAdapter lotterRecyclerViewAdapter;
    private List<LotterSortInfo.ResultBean> list = new ArrayList<>();
    private LotterPresenter lotterPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lotter);
        ButterKnife.bind(this);
        setToolbar(lotterToolbar);
        initRecyclerView();
    }

    /**
     * 初始化RecyclerView
     */
    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        lotterRecyclerView.setLayoutManager(gridLayoutManager);
        lotterRecyclerViewAdapter = new LotterRecyclerViewAdapter(this, list);
        lotterRecyclerView.setAdapter(lotterRecyclerViewAdapter);
        lotterPresenter = new LotterPresenter(this);
        lotterRecyclerViewAdapter.setItemClickCallback(this);
        lotterToolbar.setNavigationOnClickListener(v -> finish());
        lotterState.showLoading();
        lotterPresenter.getLotterSort();
    }

    @Override
    public void onLotterLoad(LotterSortInfo lotterSortInfo) {
        if (lotterSortInfo.getResult() != null) {
            if (lotterSortInfo.getResult().size() > 0) {
                list.clear();
                list.addAll(lotterSortInfo.getResult());
                lotterRecyclerViewAdapter.notifyDataSetChanged();
                lotterState.showContent();
            }else {
                lotterState.showEmpty("暂无支持的彩票种类");
            }
        }else {
            switchState(BaseContants.TYPE_ERROR, lotterState, "数据异常",
                    v -> lotterPresenter.getLotterSort());
        }
    }

    @Override
    public void onLotterFail(String reason) {
        lotterState.showError(R.drawable.ic_error, reason, "", "再试一次",
                v -> lotterPresenter.getLotterSort());
    }

    @Override
    public void onNetError() {
        lotterState.showNetError("网络连接失败，请检查你的网络",
                v -> lotterPresenter.getLotterSort());
    }

    @Override
    public void onItemClick(int postion) {
        Intent intent = new Intent();
        intent.setClass(LotterActivity.this,LotterResultActivity.class);
        intent.putExtra("lotterid",list.get(postion).getLottery_id());
        startActivity(intent);
    }
}
