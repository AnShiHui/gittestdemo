package com.ann.anlife.presenter;

import com.alibaba.fastjson.JSON;
import com.ann.anlife.base.BaseImpl;
import com.ann.anlife.http.HttpImpl;
import com.ann.anlife.http.HttpUrls;
import com.ann.anlife.interfaces.OnHttpCallback;
import com.ann.anlife.model.LotterHistoryInfo;
import com.ann.anlife.model.LotterResultInfo;
import com.ann.anlife.model.LotterSortInfo;
import com.ann.anlife.viewmodel.BaseLotterHisView;
import com.ann.anlife.viewmodel.BaseLotterResultView;
import com.ann.anlife.viewmodel.BaseLotterSortView;

import java.util.Map;

import androidx.annotation.Nullable;
import io.reactivex.disposables.CompositeDisposable;

/**
 * author Ann
 * description
 * createtime 2019/11/23
 */
public class LotterPresenter extends BaseImpl {

    private BaseLotterSortView lotterSortView;
    private BaseLotterResultView baseLotterResultView;
    private BaseLotterHisView baseLotterHisView;
    private HttpImpl mHttp;
    private CompositeDisposable compositeDisposable = getCompositeDisposabe();

    public LotterPresenter(BaseLotterSortView lotterSortView) {
        this.lotterSortView = lotterSortView;
        if (mHttp == null){
            mHttp = new HttpImpl(compositeDisposable);
        }
    }

    public LotterPresenter(BaseLotterResultView baseLotterResultView) {
        this.baseLotterResultView = baseLotterResultView;
        if (mHttp == null){
            mHttp = new HttpImpl(compositeDisposable);
        }
    }

    public LotterPresenter(BaseLotterHisView baseLotterHisView) {
        this.baseLotterHisView = baseLotterHisView;
        if (mHttp == null){
            mHttp = new HttpImpl(compositeDisposable);
        }
    }

    public void getLotterSort(){
        Map<String,Object> map = getHashMap();
        map.put("key","0cf8b678c05d4b8b0039609966528843");
        mHttp.startRequest(HttpUrls.mLotterSortUrl, map, new OnHttpCallback() {
            @Override
            public void onSuccess(String result) {
                lotterSortView.onLotterLoad(JSON.parseObject(result, LotterSortInfo.class));
            }

            @Override
            public void onFail(String reason) {
                lotterSortView.onLotterFail(reason);
            }

            @Override
            public void onNetError(String error) {
                lotterSortView.onNetError();
            }
        });
    }

    public void getLotterResult(String id, @Nullable String date){
        Map<String,Object> map = getHashMap();
        map.put("key","0cf8b678c05d4b8b0039609966528843");
        map.put("lottery_id",id);
        if (date != null){
            map.put("lottery_no",date);
        }
        mHttp.startRequest(HttpUrls.mLotterResultUrl, map, new OnHttpCallback() {
                @Override
                public void onSuccess(String result) {
                    baseLotterResultView.onResultLoad(JSON.parseObject(result, LotterResultInfo.class));
                }

                @Override
                public void onFail(String reason) {
                    baseLotterResultView.onResultFail(reason);
                }

            @Override
            public void onNetError(String error) {
                baseLotterResultView.onNetError();
            }
        });
    }

    public void getLotterHistory(String id){
        Map<String,Object> map = getHashMap();
        map.put("key","0cf8b678c05d4b8b0039609966528843");
        map.put("lottery_id",id);
        mHttp.startRequest(HttpUrls.mLotterHistoryUrl, map, new OnHttpCallback() {
            @Override
            public void onSuccess(String result) {
                baseLotterHisView.onHistoryLoad(JSON.parseObject(result, LotterHistoryInfo.class));
            }

            @Override
            public void onFail(String reason) {
                baseLotterHisView.onHistoryFail(reason);
            }

            @Override
            public void onNetError(String error) {
                baseLotterHisView.onNetError();
            }
        });
    }

    @Override
    public void onRelease() {
        if (compositeDisposable != null){
            compositeDisposable.clear();
        }
    }
}
