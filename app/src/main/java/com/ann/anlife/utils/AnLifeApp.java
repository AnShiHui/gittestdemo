package com.ann.anlife.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.tencent.bugly.Bugly;

import org.litepal.LitePal;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.multidex.MultiDex;

/**
 * author Ann
 * description
 * createtime 2019/9/18
 */
public class AnLifeApp extends Application {

    public static AnLifeApp anLifeApp;
    private int count;

    @Override
    public void onCreate() {
        super.onCreate();
        Bugly.init(getApplicationContext(), "6c384df682", false);
        anLifeApp = this;
        LitePal.initialize(this);
        /*
         * 所有Activity监听
         */
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(@NonNull Activity activity) {
                count++;
                if (count == 1){
                    LogUtil.d("Application is In Foreground");
                }
            }

            @Override
            public void onActivityResumed(@NonNull Activity activity) {

            }

            @Override
            public void onActivityPaused(@NonNull Activity activity) {

            }

            @Override
            public void onActivityStopped(@NonNull Activity activity) {
                count--;
                if (count == 0){
                    LogUtil.d("Application is In Background");
                }
            }

            @Override
            public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(@NonNull Activity activity) {

            }
        });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static AnLifeApp getInstance() {
        return anLifeApp;
    }

}
