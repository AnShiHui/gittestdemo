package com.ann.anlife.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ann.anlife.R
import com.ann.anlife.adapter.SettingAdapter
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity() {
    var dataList:ArrayList<String> = arrayListOf()
    lateinit var mAdapter:SettingAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        initView();
    }

    init {
        for (i in 0 ..10){
            dataList.add("设置的当前item $i")
        }
    }

    private fun initView() {
        setSupportActionBar(setting_toolbar)
        actionBar?.setDisplayHomeAsUpEnabled(true);
        setting_toolbar.setNavigationOnClickListener{
            finish()
        }
        mAdapter = SettingAdapter(this,dataList)
        setting_recyclerView.let {
            it.layoutManager = LinearLayoutManager(this,RecyclerView.VERTICAL,false)
            it.adapter = mAdapter
        }
    }
}
