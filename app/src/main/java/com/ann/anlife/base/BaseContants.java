package com.ann.anlife.base;

/**
 * author Ann
 * description
 * createtime 2019/10/5
 */
public class BaseContants {

    /*主页地图相关权限申请的code*/
    public static final int MAIN_PERMISSION_CODE = 1001;

    //loading 状态
    public static final int TYPE_LOADING = 1002;

    //空数据状态
    public static final int TYPE_EMPTY = 1003;

    //显示内容
    public static final int TYPE_CONTENT = 1004;

    //错误状态
    public static final int TYPE_ERROR = 1005;

    //网络错误状态
    public static final int TYPE_NET = 1006;
}