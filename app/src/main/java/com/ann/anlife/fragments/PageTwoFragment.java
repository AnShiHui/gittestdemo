package com.ann.anlife.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ann.anlife.R;
import com.ann.anlife.activity.WebViewActivity;
import com.ann.anlife.adapter.WechatRecyclerViewAdapter;
import com.ann.anlife.base.BaseFragmentImpl;
import com.ann.anlife.interfaces.OnRecyclerLoadMoreCallback;
import com.ann.anlife.interfaces.OnRecyclerViewItemClickCallback;
import com.ann.anlife.model.WeChatInfo;
import com.ann.anlife.presenter.WechatPresenter;
import com.ann.anlife.view.ProgressConstraintLayout;
import com.ann.anlife.viewmodel.BaseWechatView;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 精选
 */
public class PageTwoFragment extends BaseFragmentImpl implements BaseWechatView, OnRecyclerViewItemClickCallback {

    @BindView(R.id.fragment_two_recyclerview)
    RecyclerView fragmentTwoRecyclerview;
    @BindView(R.id.fragment_two_refresh)
    SwipeRefreshLayout fragmentTwoRefresh;
    @BindView(R.id.fragment_two_state)
    ProgressConstraintLayout fragmentTwoState;
    private Unbinder unbinder;
    private List<WeChatInfo.ResultBean.ListBean> listBeans = new ArrayList<>();
    private WechatRecyclerViewAdapter adapter;
    private int page = 1;
    private WechatPresenter wechatPresenter;

    @Override
    public View initView(LayoutInflater layoutInflater, ViewGroup container, Bundle saveInstanceState) {
        View view = layoutInflater.inflate(R.layout.fragment_page_two, container, false);
        unbinder = ButterKnife.bind(this, view);
        initRecyclerView();
        return view;
    }

    private void initRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getFragmentContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        fragmentTwoRecyclerview.setLayoutManager(linearLayoutManager);
        adapter = new WechatRecyclerViewAdapter(listBeans, getFragmentContext());
        adapter.setItemClickCallback(this);
        fragmentTwoRefresh.setColorSchemeResources(R.color.red);
        fragmentTwoRecyclerview.setAdapter(adapter);
        wechatPresenter = new WechatPresenter(this);
        fragmentTwoState.showLoading();
        wechatPresenter.getWechatInfo(page, 30);
        fragmentTwoRecyclerview.addOnScrollListener(new OnRecyclerLoadMoreCallback() {
            @Override
            public void onLoadMore() {
                page++;
                wechatPresenter.getWechatMoreInfo(page, 30);
            }
        });
        fragmentTwoRefresh.setOnRefreshListener(() -> {
            page = 1;
            wechatPresenter.getWechatInfo(page, 30);
        });
    }

    @Override
    public void onLazyLoad() {
    }

    @Override
    public void onWechatLoad(WeChatInfo weChatInfo) {
        if (fragmentTwoState.isLoadingCurrentState()){
            fragmentTwoState.showContent();
        }
        fragmentTwoRefresh.setRefreshing(false);
        listBeans.clear();
        listBeans.addAll(weChatInfo.getResult().getList());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onWechatLoadFail(String reason) {
        if (page > 1){
            page--;
        }
        fragmentTwoRefresh.setRefreshing(false);
            fragmentTwoState.showError(R.drawable.ic_error, reason, "", "再试一次", v -> {
                if (page == 1){
                    wechatPresenter.getWechatInfo(page,20);
                }else {
                    wechatPresenter.getWechatMoreInfo(page,20);
                }
            });
        adapter.onFinished(false);
    }

    @Override
    public void onWechatLoadMore(WeChatInfo weChatInfo) {
        listBeans.addAll(weChatInfo.getResult().getList());
        adapter.onFinished(true);
    }

    @Override
    public void onNetError() {
        if (page > 1){
            page--;
        }
        fragmentTwoState.showNetError( "网络错误，请检查你的网络", v -> {
            if (page == 1){
                wechatPresenter.getWechatInfo(page,20);
            }else {
                wechatPresenter.getWechatMoreInfo(page,20);
            }
        });
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onItemClick(int postion) {
        if (postion < listBeans.size()) {
            Intent intent = new Intent();
            intent.putExtra("url", listBeans.get(postion).getUrl());
            intent.setClass(getFragmentContext(), WebViewActivity.class);
            startActivity(intent);
        }
    }
}
