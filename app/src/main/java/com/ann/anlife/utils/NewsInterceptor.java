package com.ann.anlife.utils;

import android.content.Context;

import com.alibaba.fastjson.JSON;
import com.ann.anlife.model.ResultInfo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

import static com.alibaba.fastjson.util.IOUtils.UTF8;

/**
 * author Ann
 * description 新闻资讯网络拦截器
 * createtime 2019/9/19
 */
public class NewsInterceptor implements Interceptor {

    private Context context;

    public NewsInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        //有网和没有网都是先读缓存
        Request request = chain.request();
        Response response = chain.proceed(request);
        ResponseBody responseBody = response.body();
        String respBody = null;
        if(responseBody != null) {
            BufferedSource source = responseBody.source();
            source.request(Long.MAX_VALUE);
            Buffer buffer = source.buffer();

            Charset charset = UTF8;
            MediaType contentType = responseBody.contentType();
            if (contentType != null) {
                try {
                    charset = contentType.charset(UTF8);
                } catch (UnsupportedCharsetException e) {
                    e.printStackTrace();
                }
            }
            if (charset != null) {
                respBody = buffer.clone().readString(charset);
            }
            LogUtil.d("result info "+respBody);
        }
        if (JSON.isValid(respBody) && JSON.parseObject(respBody, ResultInfo.class).getError_code() != 0){
            Request cacheReq = request.newBuilder()
                    .cacheControl(new CacheControl.Builder()
                            .maxStale(30, TimeUnit.DAYS)
                            .build())
                    .build();
            LogUtil.d("get cache "+cacheReq.toString());
            response = chain.proceed(cacheReq);
            return response.newBuilder().build();
        }else {
            if (!NetUtils.hasNetWorkConection(context)){
                Request request1 = request.newBuilder()
                        .cacheControl(CacheControl.FORCE_CACHE)
                        .build();
                return chain.proceed(request1).newBuilder().build();
            }else {
                Response response1 = chain.proceed(request);
                return response1.newBuilder().header("Cache-Control", "public,only-if-cached,max-stale=" + 30 * 24 * 60 * 60)
                        .removeHeader("Pragma")
                        .build();
            }
        }
    }
}
