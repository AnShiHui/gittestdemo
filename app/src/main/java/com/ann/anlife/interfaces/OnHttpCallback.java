package com.ann.anlife.interfaces;

/**
 * author Ann
 * description
 * createtime 2019/11/23
 */
public interface OnHttpCallback {

    void onSuccess(String result);

    void onFail(String reason);

    void onNetError(String error);
}
