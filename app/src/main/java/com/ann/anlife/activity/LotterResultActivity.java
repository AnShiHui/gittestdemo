package com.ann.anlife.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ann.anlife.R;
import com.ann.anlife.adapter.LotterResultRecyclerViewAdapter;
import com.ann.anlife.base.BaseActivity;
import com.ann.anlife.base.BaseContants;
import com.ann.anlife.model.LotterResultInfo;
import com.ann.anlife.presenter.LotterPresenter;
import com.ann.anlife.utils.LogUtil;
import com.ann.anlife.view.ProgressConstraintLayout;
import com.ann.anlife.viewmodel.BaseLotterResultView;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LotterResultActivity extends BaseActivity implements BaseLotterResultView {

    @BindView(R.id.lotter_result_toolbar)
    Toolbar lotterResultToolbar;
    @BindView(R.id.lotter_result_rec)
    RecyclerView lotterResultRec;
    @BindView(R.id.lotter_result_state)
    ProgressConstraintLayout lotterResultState;
    @BindView(R.id.lotter_name)
    AppCompatTextView lotterName;
    @BindView(R.id.lotter_num)
    AppCompatTextView lotterNum;
    @BindView(R.id.lotter_no)
    AppCompatTextView lotterNo;
    @BindView(R.id.lotter_date)
    AppCompatTextView lotterDate;
    @BindView(R.id.lotter_deadline)
    AppCompatTextView lotterDeadline;
    @BindView(R.id.lotter_money)
    AppCompatTextView lotterMoney;
    @BindView(R.id.lotter_pool)
    AppCompatTextView lotterPool;
    private LotterResultRecyclerViewAdapter resultRecyclerViewAdapter;
    private List<LotterResultInfo.ResultBean.LotteryPrizeBean> list = new ArrayList<>();
    private LotterPresenter lotterPresenter;
    private String id = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lotter_result);
        ButterKnife.bind(this);
        setToolbar(lotterResultToolbar);
        initRecyclerView();
    }

    private void initRecyclerView() {
        id = getIntent().getStringExtra("lotterid");
        lotterPresenter = new LotterPresenter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        lotterResultRec.setLayoutManager(linearLayoutManager);
        resultRecyclerViewAdapter = new LotterResultRecyclerViewAdapter(this, list);
        lotterResultRec.setAdapter(resultRecyclerViewAdapter);
        lotterResultState.showLoading();
        lotterPresenter.getLotterResult(id, null);
        lotterResultToolbar.setNavigationOnClickListener(v -> finish());
        lotterResultToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent();
                intent.setClass(LotterResultActivity.this,LotterHistoryActivity.class);
                intent.putExtra("lotterid",id);
                startActivity(intent);
                return true;
            }
        });
    }

    @Override
    public void onResultLoad(LotterResultInfo info) {
        if (info.getResult() != null && info.getResult().getLottery_prize() != null) {
            List<LotterResultInfo.ResultBean.LotteryPrizeBean> lotteryPrize = info.getResult().getLottery_prize();
            LotterResultInfo.ResultBean result = info.getResult();
            if (lotteryPrize.size() > 0) {
                list.clear();
                list.addAll(lotteryPrize);
                lotterName.setText(result.getLottery_name());
                lotterNum.setText("开奖号码："+result.getLottery_res());
                LogUtil.d("arrays "+result.getLottery_res().replace(",","").toCharArray().length);
                lotterMoney.setText("本期销售额：¥ "+result.getLottery_sale_amount());
                lotterDate.setText("开奖日期："+result.getLottery_date());
                lotterDeadline.setText("兑换截止日期："+result.getLottery_exdate());
                lotterNo.setText("开奖期号："+result.getLottery_no());
                lotterPool.setText("奖池滚存：¥ "+result.getLottery_pool_amount());
                resultRecyclerViewAdapter.notifyDataSetChanged();
                lotterResultState.showContent();
            } else {
                switchState(BaseContants.TYPE_EMPTY, lotterResultState, "暂无查询结果",
                        v -> lotterPresenter.getLotterResult(id, null));
            }
        } else {
            switchState(BaseContants.TYPE_ERROR, lotterResultState, "服务器返回数据异常",
                    v -> lotterPresenter.getLotterResult(id, null));
        }
    }

    @Override
    public void onResultFail(String reason) {
        switchState(BaseContants.TYPE_ERROR, lotterResultState, reason,
                v -> lotterPresenter.getLotterResult(id, null));
    }

    @Override
    public void onNetError() {
        switchState(BaseContants.TYPE_NET, lotterResultState, "网络错误，请检查您的网络",
                v -> lotterPresenter.getLotterResult(id, null));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.lotter_result_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }
}
