package com.ann.anlife.interfaces;

/**
 * author Ann
 * description
 * createtime 2019/9/19
 */
public interface OnRecyclerViewItemClickCallback {

    void onItemClick(int postion);
}
