package com.ann.anlife.viewmodel;

import com.ann.anlife.base.BaseView;
import com.ann.anlife.model.WeChatInfo;

/**
 * author Ann
 * description
 * createtime 2019/9/20
 */
public interface BaseWechatView extends BaseView {

    void onWechatLoad(WeChatInfo weChatInfo);

    void onWechatLoadFail(String reason);

    void onWechatLoadMore(WeChatInfo weChatInfo);
}
