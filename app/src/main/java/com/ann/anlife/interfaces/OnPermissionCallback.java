package com.ann.anlife.interfaces;

/**
 * author Ann
 * description 权限一次性申请接口
 * createtime 2019/10/16
 */
public interface OnPermissionCallback {

    void onPermissionGanted();

    void onPermissionDenied();
}
