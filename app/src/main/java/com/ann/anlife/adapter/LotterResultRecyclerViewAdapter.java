package com.ann.anlife.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ann.anlife.R;
import com.ann.anlife.model.LotterResultInfo;
import com.ann.anlife.utils.LogUtil;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * author Ann
 * description
 * createtime 2019/11/24
 */
public class LotterResultRecyclerViewAdapter extends RecyclerView.Adapter<LotterResultRecyclerViewAdapter.LotterResultolder> {


    private Unbinder unbinder;
    private Context context;
    private List<LotterResultInfo.ResultBean.LotteryPrizeBean> list;

    public LotterResultRecyclerViewAdapter(Context context, List<LotterResultInfo.ResultBean.LotteryPrizeBean> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public LotterResultolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_lotter_result_adapter, parent, false);
        return new LotterResultolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LotterResultolder holder, int position) {
        if (list.size() > 0 && list.size() > position){
            holder.lotterResultName.setText(list.get(position).getPrize_name());
            holder.lotterResultMoney.setText("中奖金额：¥ "+list.get(position).getPrize_amount());
            holder.lotterResultCount.setText("中奖数量："+list.get(position).getPrize_num());
            holder.lotterResultCondition.setText("中奖条件："+list.get(position).getPrize_require());
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class LotterResultolder extends RecyclerView.ViewHolder {
        @BindView(R.id.lotter_result_name)
        AppCompatTextView lotterResultName;
        @BindView(R.id.lotter_result_count)
        AppCompatTextView lotterResultCount;
        @BindView(R.id.lotter_result_money)
        AppCompatTextView lotterResultMoney;
        @BindView(R.id.lotter_result_condition)
        AppCompatTextView lotterResultCondition;
        LotterResultolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        unbinder.unbind();
    }
}
