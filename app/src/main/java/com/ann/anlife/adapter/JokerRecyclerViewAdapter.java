package com.ann.anlife.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ann.anlife.R;
import com.ann.anlife.model.JokerInfo;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * author Ann
 * description
 * createtime 2019/11/24
 */
public class JokerRecyclerViewAdapter extends RecyclerView.Adapter<JokerRecyclerViewAdapter.JokerHolder> {

    private Unbinder unbinder;
    private Context mContext;
    private List<JokerInfo.ResultBean.DataBean> list;

    public JokerRecyclerViewAdapter(Context mContext, List<JokerInfo.ResultBean.DataBean> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @NonNull
    @Override
    public JokerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_joker_adapter, parent, false);
        return new JokerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JokerHolder holder, int position) {
        if (list != null && list.size() > position){
            holder.jokerAdapterContent.setText(list.get(position).getContent());
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class JokerHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.joker_adapter_content)
        AppCompatTextView jokerAdapterContent;
        @BindView(R.id.joker_adapter_card)
        CardView jokerAdapterCard;
        JokerHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        unbinder.unbind();
    }
}
