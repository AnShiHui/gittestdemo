package com.ann.anlife.interfaces;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_DRAGGING;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_SETTLING;

/**
 * author Ann
 * description recyclerView上拉加载
 * createtime 2019/9/19
 */
public abstract class OnRecyclerLoadMoreCallback extends RecyclerView.OnScrollListener {

    private boolean isScolled;
    private int countItem;
    private int lastItem;

    @Override
    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        isScolled = newState == SCROLL_STATE_DRAGGING || newState == SCROLL_STATE_SETTLING;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            countItem = layoutManager.getItemCount();
            lastItem = ((LinearLayoutManager) layoutManager).findLastCompletelyVisibleItemPosition();
        }
        if (isScolled && countItem != lastItem && lastItem == countItem - 1) {
            onLoadMore();
        }
    }

    public abstract void onLoadMore();
}
