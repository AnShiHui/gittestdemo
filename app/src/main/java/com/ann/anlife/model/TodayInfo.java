package com.ann.anlife.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * author Ann
 * description
 * createtime 2019/9/20
 */
public class TodayInfo {


    /**
     * reason : success
     * result : [{"day":"9/19","date":"1123年09月19日","title":"金太祖完颜阿骨打逝世","e_id":"10711"},{"day":"9/19","date":"1783年09月19日","title":"孟戈菲兄弟在凡尔赛宫的进行气球升空实验","e_id":"10712"},{"day":"9/19","date":"1857年09月19日","title":"德里在英军攻击下陷落","e_id":"10713"},{"day":"9/19","date":"1894年09月19日","title":"洋务派创办的上海华盛纺织总厂开工","e_id":"10714"},{"day":"9/19","date":"1921年09月19日","title":"摩洛哥北部独立，成立里夫共和国","e_id":"10715"},{"day":"9/19","date":"1932年09月19日","title":"柳直荀被\u201c左\u201d倾路线杀害","e_id":"10716"},{"day":"9/19","date":"1937年09月19日","title":"日本用非法手段\u201c美国制造\u201d与美国争夺南美市场","e_id":"10717"},{"day":"9/19","date":"1944年09月19日","title":"保加利亚退出轴心国","e_id":"10718"},{"day":"9/19","date":"1949年09月19日","title":"董其武率部于绥远起义","e_id":"10719"},{"day":"9/19","date":"1956年09月19日","title":"埃及收回对苏伊士运河主权","e_id":"10720"},{"day":"9/19","date":"1959年09月19日","title":"阿根廷总统庇隆被推翻","e_id":"10721"},{"day":"9/19","date":"1962年09月19日","title":"美国政治家司徒雷登逝世","e_id":"10722"},{"day":"9/19","date":"1972年09月19日","title":"我国与多哥建交","e_id":"10723"},{"day":"9/19","date":"1981年09月19日","title":"邓小平检阅华北军事演习","e_id":"10724"},{"day":"9/19","date":"1982年09月19日","title":"人类历史上第一张电脑笑脸就此诞生","e_id":"10725"},{"day":"9/19","date":"1983年09月19日","title":"圣尼独立","e_id":"10726"},{"day":"9/19","date":"1985年09月19日","title":"墨西哥发生强烈地震","e_id":"10727"},{"day":"9/19","date":"1987年09月19日","title":"我国第一部物价管理条例正式颁布实施","e_id":"10728"},{"day":"9/19","date":"1988年09月19日","title":"庄泳为我国夺得首枚奥运游泳奖牌","e_id":"10729"},{"day":"9/19","date":"1988年09月19日","title":"苏联使冷冻90年的两栖动物复苏","e_id":"10730"},{"day":"9/19","date":"1989年09月19日","title":"法国联合航空公司客机在尼日尔上空爆炸","e_id":"10731"},{"day":"9/19","date":"1991年09月19日","title":"首批\u201c中国驰名商标\u201d产生","e_id":"10732"},{"day":"9/19","date":"1991年09月19日","title":"安理会通过允许伊拉克以石油换食品决议","e_id":"10733"},{"day":"9/19","date":"1991年09月19日","title":"第九届大众电视金鹰奖揭晓","e_id":"10734"},{"day":"9/19","date":"1994年09月19日","title":"美国出兵海地","e_id":"10735"},{"day":"9/19","date":"1994年09月19日","title":"巴布亚新几内亚火山爆发","e_id":"10736"},{"day":"9/19","date":"1995年09月19日","title":"上海金卡工程ATM联网在全国率先开通","e_id":"10737"},{"day":"9/19","date":"1996年09月19日","title":"联合国贸发会议发表贸易和发展报告","e_id":"10738"},{"day":"9/19","date":"1997年09月19日","title":"中共十五届一中全会产生中央领导机构","e_id":"10739"},{"day":"9/19","date":"1997年09月19日","title":"我国首次派车手参赛汽车拉力世锦赛","e_id":"10740"},{"day":"9/19","date":"1998年09月19日","title":"我国再获两项联合国人居奖","e_id":"10741"},{"day":"9/19","date":"2014年09月19日","title":"韩国仁川亚运会","e_id":"10742"},{"day":"9/19","date":"2014年09月19日","title":"阿里巴巴纽交所上市","e_id":"10743"},{"day":"9/19","date":"2015年09月19日","title":"日本通过安保法 战后政策大转变","e_id":"10744"},{"day":"9/19","date":"2015年09月19日","title":"第30届中国电影金鸡奖在吉林揭晓","e_id":"10745"}]
     * error_code : 0
     */

    private String reason;
    private int error_code;
    private List<ResultBean> result;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean implements Parcelable {
        /**
         * day : 9/19
         * date : 1123年09月19日
         * title : 金太祖完颜阿骨打逝世
         * e_id : 10711
         */

        private String day;
        private String date;
        private String title;
        private String e_id;

        public ResultBean() {
        }

        protected ResultBean(Parcel in) {
            day = in.readString();
            date = in.readString();
            title = in.readString();
            e_id = in.readString();
        }

        public static final Creator<ResultBean> CREATOR = new Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel in) {
                return new ResultBean(in);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getE_id() {
            return e_id;
        }

        public void setE_id(String e_id) {
            this.e_id = e_id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(day);
            parcel.writeString(date);
            parcel.writeString(title);
            parcel.writeString(e_id);
        }
    }
}
