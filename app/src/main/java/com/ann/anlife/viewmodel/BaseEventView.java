package com.ann.anlife.viewmodel;

import com.ann.anlife.base.BaseView;
import com.ann.anlife.model.EventDetailInfo;

/**
 * author Ann
 * description
 * createtime 2019/9/20
 */
public interface BaseEventView extends BaseView {

    void onEventDetailLoad(EventDetailInfo eventDetailInfo);

    void onEventLoadFail(String reason);
}
