package com.ann.anlife.viewmodel;

import com.ann.anlife.base.BaseView;
import com.ann.anlife.model.JokerInfo;

/**
 * author Ann
 * description
 * createtime 2019/11/24
 */
public interface BaseJokerView extends BaseView {

    void onJokerLoad(JokerInfo jokerInfo);

    void onJokerFail(String reason);
}
