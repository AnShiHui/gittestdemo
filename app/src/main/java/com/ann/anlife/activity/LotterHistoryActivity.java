package com.ann.anlife.activity;

import android.os.Bundle;
import android.view.View;

import com.ann.anlife.R;
import com.ann.anlife.adapter.LotterHistoryRecyclerViewAdapter;
import com.ann.anlife.base.BaseActivity;
import com.ann.anlife.base.BaseContants;
import com.ann.anlife.model.LotterHistoryInfo;
import com.ann.anlife.presenter.LotterPresenter;
import com.ann.anlife.view.ProgressConstraintLayout;
import com.ann.anlife.viewmodel.BaseLotterHisView;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LotterHistoryActivity extends BaseActivity implements BaseLotterHisView {

    @BindView(R.id.lotter_his_toolbar)
    Toolbar lotterHisToolbar;
    @BindView(R.id.lotter_his_rec)
    RecyclerView lotterHisRec;
    @BindView(R.id.lotter_his_state)
    ProgressConstraintLayout lotterHisState;
    private LotterHistoryRecyclerViewAdapter historyRecyclerViewAdapter;
    private List<LotterHistoryInfo.ResultBean.LotteryResListBean> listBeans = new ArrayList<>();
    private LotterPresenter lotterPresenter;
    private String id = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lotter_history);
        ButterKnife.bind(this);
        setToolbar(lotterHisToolbar);
        initRecyclerView();
    }

    private void initRecyclerView() {
        id = getIntent().getStringExtra("lotterid");
        lotterPresenter = new LotterPresenter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        lotterHisRec.setLayoutManager(linearLayoutManager);
        historyRecyclerViewAdapter = new LotterHistoryRecyclerViewAdapter(this,listBeans);
        lotterHisRec.setAdapter(historyRecyclerViewAdapter);
        lotterHisToolbar.setNavigationOnClickListener(v -> finish());
        lotterHisState.showLoading();
        lotterPresenter.getLotterHistory(id);
    }

    @Override
    public void onHistoryLoad(LotterHistoryInfo historyInfo) {
        if (historyInfo.getResult() != null && historyInfo.getResult().getLotteryResList() != null){
            List<LotterHistoryInfo.ResultBean.LotteryResListBean> lotteryResList = historyInfo.getResult().getLotteryResList();
            if (lotteryResList.size() > 0 ){
                listBeans.addAll(lotteryResList);
                lotterHisState.showContent();
                historyRecyclerViewAdapter.notifyDataSetChanged();
            }else {
                lotterHisState.showEmpty("暂无历史结果");
            }
        }else {
            switchState(BaseContants.TYPE_ERROR, lotterHisState, "服务器返回数据异常",
                    v -> lotterPresenter.getLotterHistory(id));
        }
    }

    @Override
    public void onHistoryFail(String reason) {
        switchState(BaseContants.TYPE_ERROR, lotterHisState, reason,
                v -> lotterPresenter.getLotterHistory(id));
    }

    @Override
    public void onNetError() {
        switchState(BaseContants.TYPE_NET, lotterHisState, "网络错误，请检查你的网络",
                v -> lotterPresenter.getLotterHistory(id));
    }
}
