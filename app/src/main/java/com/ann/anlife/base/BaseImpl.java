package com.ann.anlife.base;


import android.accounts.NetworkErrorException;

import com.alibaba.fastjson.JSON;
import com.ann.anlife.http.RetrofitApis;
import com.ann.anlife.interfaces.OnHttpCallback;
import com.ann.anlife.model.ResultInfo;
import com.ann.anlife.utils.AnLifeApp;
import com.ann.anlife.utils.NewsInterceptor;

import java.io.File;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

/**
 * author Ann
 * description
 * createtime 2019/9/17
 */
public abstract class BaseImpl implements LifecycleObserver {

    private Retrofit retrofit;
    private OkHttpClient okHttpClient;

    //获取请求端
    protected Retrofit getRetrofitClient(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(getOkhttpClient())
                    .baseUrl(BaseHttpInfo.BASE_URL)
                    .build();
        }
        return retrofit;
    }

    //获取okhttpclient
    private OkHttpClient getOkhttpClient() {
        Cache cache = new Cache(new File(AnLifeApp.getInstance().getExternalCacheDir(),
                "news_cache"),1024 * 1024 *10);
        if (okHttpClient == null){
            okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(10000, TimeUnit.SECONDS)
                    .writeTimeout(10000,TimeUnit.SECONDS)
                    .addInterceptor(new NewsInterceptor(AnLifeApp.getInstance().getApplicationContext()))
                    .cache(cache)
                    .build();
        }
        return okHttpClient;
    }

    protected Map<String,Object> getHashMap(){
        return new HashMap<>();
    }

    /**
     * 订阅纽带容器
     */
    protected CompositeDisposable getCompositeDisposabe(){
        return new CompositeDisposable();
    }

    /**
     * 获取请求体
     */
    protected RetrofitApis getApis(){
        return getRetrofitClient().create(RetrofitApis.class);
    }

    /**
     * 判断是否请求成功
     * @param result json字符串
     */
    public boolean isSuccess(String result){
        return JSON.isValid(result) && JSON.parseObject(result, ResultInfo.class) != null
                && Math.round(JSON.parseObject(result, ResultInfo.class).getError_code()) == 0;
    }

    /**
     * 获取服务器返回的msg
     * @param result json字符串
     */
    public String getErrorMsg(String result){
        if (JSON.isValid(result) && JSON.parseObject(result,ResultInfo.class).getReason() != null){
            return JSON.parseObject(result,ResultInfo.class).getReason();
        }
        return "服务器返回数据异常";
    }

    /**
     * 转换异常为String
     * @param e {@link #okHttpClient}
     */
    public static void getErrorType(Throwable e, OnHttpCallback callback){
        if (e instanceof ConnectException || e instanceof NetworkErrorException ||e instanceof UnknownHostException){
           callback.onNetError("网络连接异常，请检查您的网络");
        }else if (e instanceof TimeoutException || e instanceof SocketTimeoutException){
            callback.onFail("连接服务器超时，请稍后重试");
        }else {
            callback.onFail("未知错误");
        }
    }

    /**
     * 解除订阅
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public abstract void onRelease();
}
