package com.ann.anlife.viewmodel;

import com.ann.anlife.base.BaseView;
import com.ann.anlife.model.LotterSortInfo;

/**
 * author Ann
 * description
 * createtime 2019/11/23
 */
public interface BaseLotterSortView extends BaseView {

    void onLotterLoad(LotterSortInfo lotterSortInfo);

    void onLotterFail(String reason);
}
