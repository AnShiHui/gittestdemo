package com.ann.anlife.presenter;

import com.alibaba.fastjson.JSON;
import com.ann.anlife.base.BaseImpl;
import com.ann.anlife.http.HttpImpl;
import com.ann.anlife.http.HttpUrls;
import com.ann.anlife.interfaces.OnHttpCallback;
import com.ann.anlife.model.LaoHuangLiInfo;
import com.ann.anlife.viewmodel.BaseLaoHuangLiView;

import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;

public class LaoHuangliPresenter extends BaseImpl {

    private BaseLaoHuangLiView huangLiView;
    private HttpImpl http;
    private CompositeDisposable compositeDisposable = getCompositeDisposabe();

    public LaoHuangliPresenter(BaseLaoHuangLiView huangLiView, HttpImpl http) {
        this.huangLiView = huangLiView;
        this.http = new HttpImpl(compositeDisposable);
    }

    public void loadHuangLiData(String date){
        Map<String, Object> hashMap;
        hashMap = getHashMap();
        hashMap.put("key","13f16437c336cb2fe89fc63e2b0eb935");
        hashMap.put("date",date);
        http.startRequest(HttpUrls.mLaoHuangLiUrl, hashMap, new OnHttpCallback() {
            @Override
            public void onSuccess(String result) {
                if (huangLiView != null){
                    try {
                        LaoHuangLiInfo laoHuangLiInfo = JSON.parseObject(result, LaoHuangLiInfo.class);
                        huangLiView.onLaoHuangLiLoad(laoHuangLiInfo);
                    }catch (Exception e){
                        huangLiView.onLoadFail(e.toString());
                    }
                }
            }

            @Override
            public void onFail(String reason) {
                if (huangLiView != null){
                    huangLiView.onLoadFail(reason);
                }
            }

            @Override
            public void onNetError(String error) {
                if (huangLiView != null){
                    huangLiView.onLoadFail(error);
                }
            }
        });
    }

    @Override
    public void onRelease() {
        compositeDisposable.dispose();
    }
}
