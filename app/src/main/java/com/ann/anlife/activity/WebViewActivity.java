package com.ann.anlife.activity;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.ann.anlife.R;
import com.ann.anlife.base.BaseActivity;
import com.ann.anlife.utils.LogUtil;
import com.ann.anlife.view.NoScrollWebView;
import com.jaeger.library.StatusBarUtil;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.web_activity_toolbar)
    Toolbar webActivityToolbar;
    @BindView(R.id.web_line)
    ProgressBar webLine;
    @BindView(R.id.web_activity_web)
    NoScrollWebView webActivityWeb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        StatusBarUtil.setLightMode(this);
        setToolbar(webActivityToolbar);
        webViewSetting(getIntent().getStringExtra("url"));
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void webViewSetting(String htmlUrl) {
        String cachepath = Objects.requireNonNull(getExternalCacheDir()).getAbsolutePath();
        WebSettings webSettings = webActivityWeb.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(true);
        webSettings.setAppCachePath(cachepath);
        webSettings.setAppCacheEnabled(true);
        webSettings.setSupportMultipleWindows(true);
        webActivityWeb.loadUrl(htmlUrl);
        webActivityWeb.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
                return super.shouldOverrideKeyEvent(view, event);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
            }

            @Override
            public void onReceivedLoginRequest(WebView view, String realm, @Nullable String account, String args) {
                super.onReceivedLoginRequest(view, realm, account, args);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString());
                return false;
            }
        });
        webActivityWeb.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress >= 100) {
                    webLine.setVisibility(View.GONE);
                } else {
                    webLine.setProgress(newProgress);
                }
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }

            @Override
            public void onPermissionRequest(PermissionRequest request) {
                LogUtil.d("permission request");
                super.onPermissionRequest(request);
            }

            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                LogUtil.d("showFilechooser");
                return super.onShowFileChooser(webView, filePathCallback, fileChooserParams);
            }

            @Override
            public void onPermissionRequestCanceled(PermissionRequest request) {
                LogUtil.d("permission cancle");
                super.onPermissionRequestCanceled(request);
            }
        });

        webActivityToolbar.setNavigationOnClickListener(view -> finish());
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    protected void onDestroy() {
        if (webActivityWeb != null){
            webActivityWeb.clearCache(true);
            webActivityWeb.destroy();
        }
        super.onDestroy();
    }
}
