package com.ann.anlife.viewmodel;

import com.ann.anlife.base.BaseView;
import com.ann.anlife.model.TodayInfo;
import com.ann.anlife.model.WeatherInfo;

/**
 * author Ann
 * description
 * createtime 2019/9/20
 */
public interface BaseMainView extends BaseView {

    void onTodayInHistory(TodayInfo todayInfo);

    void onTodayLoadFail(String reason);

    void onWeatherLoad(WeatherInfo weatherInfo);

    void onWeatherFail(String reason);
}
