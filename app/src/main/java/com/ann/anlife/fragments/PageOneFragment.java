package com.ann.anlife.fragments;


import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import com.ann.anlife.R;
import com.ann.anlife.activity.MainActivity;
import com.ann.anlife.adapter.NewsRecyclerViewAdapter;
import com.ann.anlife.adapter.NewsViewPagerAdapter;
import com.ann.anlife.base.BaseFragmentImpl;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 首页
 */
public class PageOneFragment extends BaseFragmentImpl {

    @BindView(R.id.fragment_one_tablayout)
    TabLayout fragmentOneTablayout;
    @BindView(R.id.fragment_one_viewpager)
    ViewPager fragmentOneViewpager;
    private Unbinder unbinder;
    private List<Fragment> list = new ArrayList<>();
    private NewsViewPagerAdapter pagerAdapter;
    private ScaleAnimation scaleAnimation;

    @Override
    public View initView(LayoutInflater layoutInflater, ViewGroup container, Bundle saveInstanceState) {
        View view = layoutInflater.inflate(R.layout.fragment_page_one, container, false);
        unbinder = ButterKnife.bind(this, view);
        initTablayoutWithViewPager();
        return view;
    }

    /**
     * 设置tablayout
     */
    private void initTablayoutWithViewPager() {
        addFragments();
        pagerAdapter = new NewsViewPagerAdapter(getChildFragmentManager(),list);
        fragmentOneViewpager.setOffscreenPageLimit(2);
        fragmentOneViewpager.setAdapter(pagerAdapter);
        fragmentOneTablayout.setupWithViewPager(fragmentOneViewpager);
        fragmentOneTablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TextView textView = new TextView(getFragmentContext());
                float selectedSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, 15, getResources().getDisplayMetrics());
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,selectedSize);
                textView.setTextColor(getResources().getColor(R.color.red));
                textView.setText(tab.getText());
                tab.setCustomView(textView);
                if (scaleAnimation == null) {
                    scaleAnimation = new ScaleAnimation(1f, 1.5f, 1f, 1.5f);
                    scaleAnimation.setDuration(200);
                    scaleAnimation.setFillAfter(true);
                }
                textView.startAnimation(scaleAnimation);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getCustomView().clearAnimation();
                tab.setCustomView(null);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        TextView textView = new TextView(getFragmentContext());
        float selectedSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, 25, getResources().getDisplayMetrics());
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,selectedSize);
        textView.setTextColor(getResources().getColor(R.color.red));
        textView.setText(Objects.requireNonNull(fragmentOneTablayout.getTabAt(0)).getText());
        Objects.requireNonNull(fragmentOneTablayout.getTabAt(0)).setCustomView(textView);
    }

    /**
     * 添加fragment
     */
    private void addFragments() {
        list.add(NewsFragment.getInstance("toutiao"));
        list.add(NewsFragment.getInstance("shehui"));
        list.add(NewsFragment.getInstance("guonei"));
        list.add(NewsFragment.getInstance("guoji"));
        list.add(NewsFragment.getInstance("yule"));
        list.add(NewsFragment.getInstance("tiyu"));
        list.add(NewsFragment.getInstance("junshi"));
        list.add(NewsFragment.getInstance("keji"));
        list.add(NewsFragment.getInstance("caijing"));
        list.add(NewsFragment.getInstance("shishang"));
    }

    @Override
    public void onLazyLoad() {
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
