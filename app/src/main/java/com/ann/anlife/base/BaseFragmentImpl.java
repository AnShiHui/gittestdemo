package com.ann.anlife.base;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ann.anlife.utils.AnLifeApp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

//基础fragment类

public abstract class BaseFragmentImpl extends Fragment {

    private boolean isFirstLoad = false;
    private boolean isVisible = false;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View view = initView(inflater,container,savedInstanceState);
      isFirstLoad = true;
      if (isFirstLoad && isVisible){
          onLazyLoad();
          isFirstLoad = false;
      }
      return view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isFirstLoad && isVisible){
            onLazyLoad();
            isFirstLoad = false;
        }
    }

    public Context getFragmentContext(){
        if (context == null){
            context = AnLifeApp.getInstance().getApplicationContext();
        }
        return context;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        this.context = context;
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        context = null;
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        isFirstLoad = true;
        isVisible = false;
        super.onDestroyView();
    }

    public abstract View initView(LayoutInflater layoutInflater, ViewGroup container, Bundle saveInstanceState);

    public abstract void onLazyLoad();

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
