package com.ann.anlife.interfaces;

import com.tbruyelle.rxpermissions2.Permission;

/**
 * author Ann
 * description 分开请求权限接口
 * createtime 2019/10/16
 */
public interface OnEachPermissionCallback {

    void onPermissionNext(Permission permission);
}
