package com.ann.anlife.http;

import com.alibaba.fastjson.JSON;
import com.ann.anlife.base.BaseImpl;
import com.ann.anlife.interfaces.OnHttpCallback;
import com.ann.anlife.utils.AnLifeApp;
import com.ann.anlife.utils.LogUtil;
import com.ann.anlife.utils.NetUtils;

import java.io.IOException;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

/**
 * author Ann
 * description
 * createtime 2019/11/23
 */
public class HttpImpl extends BaseImpl{
    private CompositeDisposable compositeDisposable;

    public HttpImpl(CompositeDisposable compositeDisposable) {
        this.compositeDisposable = compositeDisposable;
    }

    /**
     * 通用请求
     * @param url 请求地址
     * @param map 参数
     */
    public void startRequest(String url, Map<String,Object> map,OnHttpCallback callback){
        if (callback == null){
            throw new NullPointerException("OnHttpCallback must not be null,please check");
        }
        if (!NetUtils.isWifiConnected(AnLifeApp.getInstance().getApplicationContext())){
            callback.onFail("网络无法连接，请检查您的网络");
        }else {
            getApis().getRequestMethod(url, map)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ResponseBody>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(ResponseBody responseBody) {
                            String result;
                            try {
                                result = responseBody.string();
                                LogUtil.d("onNext "+result);
                                if (isSuccess(result)) {
                                    callback.onSuccess(result);
                                } else callback.onFail(getErrorMsg(result));
                            } catch (IOException e) {
                                callback.onFail("服务器正忙，请稍后重试");
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            LogUtil.d(" onError "+e.toString());
                            getErrorType(e,callback);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
    }

    @Override
    public void onRelease() {
        if (compositeDisposable != null){
            compositeDisposable.clear();
        }
    }
}
