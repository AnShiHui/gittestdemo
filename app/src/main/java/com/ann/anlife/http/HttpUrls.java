package com.ann.anlife.http;

/**
 * author Ann
 * description
 * createTime 2019/11/23
 */
public class HttpUrls {

    /**
     * 头条地址
     */
    public static final String mToutiaoUrl = "http://v.juhe.cn/toutiao/index";

    /**
     * 微信精选地址
     */
    public static final String mWechatUrl = "http://v.juhe.cn/weixin/query";

    /**
     * 历史上的今天列表地址
     */
    public static final String mHistoryListUrl = "http://v.juhe.cn/todayOnhistory/queryEvent.php";

    /**
     * 历史上的今天详情地址
     */
    public static final String mHistoryDetailUrl = "http://v.juhe.cn/todayOnhistory/queryDetail.php";

    /**
     * 笑话大全（按时间更新顺序）地址
     */
    public static final String mLaughByTimeUrl = "http://v.juhe.cn/joke/content/list.php";

    /**
     * 笑话大全（最新）地址
     */
    public static final String mLaughUpdatedUrl = "http://v.juhe.cn/joke/content/text.php";

    /**
     * 笑话大全（随机）地址
     */
    public static final String mLaugRandomUrl= "http://v.juhe.cn/joke/randJoke.php";

    /**
     * QQ号码查凶吉
     */
    public static final String mQQNumLuckyUrl = "http://japi.juhe.cn/qqevaluate/qq";

    /**
     * 彩票支持种类
     */
    public static final String mLotterSortUrl = "http://apis.juhe.cn/lottery/types";

    /**
     * 开奖结果查询
     */
    public static final String mLotterResultUrl = "http://apis.juhe.cn/lottery/query";

    /**
     * 历史开奖结果查询
     */
    public static final String mLotterHistoryUrl = "http://apis.juhe.cn/lottery/history";

    /**
     * 中奖计算器
     */
    public static final String mLotterAnalzyUrl = "http://apis.juhe.cn/lottery/bonus";

    /**
     * 在线H5看电影
     */
    public static final String mH5MoviewOnlineUrl = "http://v.juhe.cn/wepiao/query";

    /**
     * 天气预报（城市）
     */
    public static final String mWeatherQueryUrl = "http://apis.juhe.cn/simpleWeather/query";

    /**
     * 天气种类查询
     */
    public static final String mWeatherSortUrl = "http://apis.juhe.cn/simpleWeather/wids";

    /**
     * 城市列表查询
     */
    public static final String mCityListUrl = "http://apis.juhe.cn/simpleWeather/cityList";

    /**
     * 星座运势
     */
    public static final String mStarLuckyUrl = "http://web.juhe.cn:8080/constellation/getAll";

    /**
     * 老黄历
     */
    public static final String mLaoHuangLiUrl = "http://v.juhe.cn/laohuangli/d";
}
