package com.ann.anlife.activity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ann.anlife.R;
import com.ann.anlife.base.BaseActivity;
import com.ann.anlife.utils.DownloadUtil;

import java.io.File;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateVersionActivity extends BaseActivity {

    @BindView(R.id.update_toolbar)
    Toolbar updateToolbar;
    @BindView(R.id.update_content)
    AppCompatTextView updateContent;
    @BindView(R.id.update_pb)
    ProgressBar updatePb;
    @BindView(R.id.update_update)
    AppCompatTextView updateUpdate;
    private static final String mDownLoadUrl = "https://b6.market.xiaomi.com/download/AppStore/0cb0a5e6747523d2e2952d8da3c8ff71a9c43f1c4/com.tencent.mobileqq.apk";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_version);
        ButterKnife.bind(this);
        setToolbar(updateToolbar);
    }

    @OnClick(R.id.update_update)
    public void onViewClicked() {
        updateUpdate.setText("正在连接...");
        updatePb.setIndeterminate(true);
        new DownloadUtil().download(mDownLoadUrl, Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath(), "AnLife.apk", new DownloadUtil.OnDownloadListener() {
            @Override
            public void onDownloadSuccess(File file) {
                install(file.getPath());
            }

            @Override
            public void onDownloading(int progress) {
                updatePb.setIndeterminate(false);
                if (progress > 0 && progress <= 100){
                    runOnUiThread(() -> {
                        updatePb.setProgress(progress);
                        updateUpdate.setText("已下载 %"+progress);
                    });
                }
            }

            @Override
            public void onDownloadFailed(Exception e) {
                runOnUiThread(() -> Toast.makeText(UpdateVersionActivity.this,e.toString(),Toast.LENGTH_LONG).show()); }
        });
    }

    /**
     * 安装apk
     * @param filePath 路径
     */
    public void install (String filePath){
        File apkFile = new File(filePath);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri contentUri = FileProvider.getUriForFile(UpdateVersionActivity.this,getApplicationContext().getPackageName()+".fileprovider",apkFile);//FileProviderUtils.getUriForFile(this,apkFile);
            intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                if (!hasInstallPermission) {
                    startInstallPermissionSettingActivity();
                }
            }
        } else {
            intent.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
        } try {
            startActivity(intent);
        } catch (Exception e) {
            runOnUiThread(() -> Toast.makeText(UpdateVersionActivity.this,"安装失败,请重试"+e.toString(),Toast.LENGTH_LONG).show());
        }
    }

    /**
     * 申请8.0安装未知来源权限
     */
    @TargetApi(Build.VERSION_CODES.O)
    public void startInstallPermissionSettingActivity() { //注意这个是8.0新API
        Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
