package com.ann.anlife.activity;

import android.os.Bundle;
import android.view.View;

import com.ann.anlife.R;
import com.ann.anlife.adapter.JokerRecyclerViewAdapter;
import com.ann.anlife.base.BaseActivity;
import com.ann.anlife.interfaces.OnRecyclerLoadMoreCallback;
import com.ann.anlife.model.JokerInfo;
import com.ann.anlife.presenter.JokerPresenter;
import com.ann.anlife.view.ProgressConstraintLayout;
import com.ann.anlife.viewmodel.BaseJokerView;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LaugherActivity extends BaseActivity implements BaseJokerView {

    @BindView(R.id.laugher_toolbar)
    Toolbar laugherToolbar;
    @BindView(R.id.laugher_rec)
    RecyclerView laugherViewpager;
    @BindView(R.id.laugher_state)
    ProgressConstraintLayout laugherState;
    private JokerRecyclerViewAdapter jokerRecyclerViewAdapter;
    private List<JokerInfo.ResultBean.DataBean> list = new ArrayList<>();
    private JokerPresenter jokerPresenter;
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laugher);
        ButterKnife.bind(this);
        setToolbar(laugherToolbar);
        initRecyclerView();
    }

    private void initRecyclerView() {
        jokerPresenter = new JokerPresenter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        laugherViewpager.setLayoutManager(linearLayoutManager);
        jokerRecyclerViewAdapter = new JokerRecyclerViewAdapter(this, list);
        laugherViewpager.setAdapter(jokerRecyclerViewAdapter);
        laugherState.showLoading();
        jokerPresenter.getJoker(page,20);
        laugherToolbar.setNavigationOnClickListener(v -> finish());
        laugherViewpager.addOnScrollListener(new OnRecyclerLoadMoreCallback() {
            @Override
            public void onLoadMore() {
                page++;
                jokerPresenter.getJoker(page,20);
            }
        });
    }

    @Override
    public void onJokerLoad(JokerInfo jokerInfo) {
        if (jokerInfo.getResult() != null && jokerInfo.getResult().getData() != null){
            List<JokerInfo.ResultBean.DataBean> data = jokerInfo.getResult().getData();
            if (data.size() > 0 ){
                if (page <= 1) {
                    list.clear();
                }
                list.addAll(data);
                laugherState.showContent();
                jokerRecyclerViewAdapter.notifyDataSetChanged();
            }else {
                if (laugherState.isLoadingCurrentState()){
                    laugherState.showEmpty("暂时没有笑话哦");
                }
            }
        }else {
            laugherState.showError(R.drawable.ic_error, "服务器返回数据异常", "", "再试一次",
                    v -> jokerPresenter.getJoker(page,20));
        }
    }

    @Override
    public void onJokerFail(String reason) {
        if (page > 1){
            page--;
        }
        laugherState.showError(R.drawable.ic_error, reason, "", "再试一次",
                v -> jokerPresenter.getJoker(page,20));
    }

    @Override
    public void onNetError() {
        if (page > 1){
            page--;
        }
        laugherState.showNetError("网络错误，请检查您的网络",
                v -> jokerPresenter.getJoker(page,20));
    }
}
