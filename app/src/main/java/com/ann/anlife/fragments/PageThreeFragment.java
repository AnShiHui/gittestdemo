package com.ann.anlife.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ann.anlife.R;
import com.ann.anlife.activity.LaugherActivity;
import com.ann.anlife.activity.LotterActivity;
import com.ann.anlife.adapter.LifeRecyclerViewAdapter;
import com.ann.anlife.base.BaseFragmentImpl;
import com.ann.anlife.interfaces.OnRecyclerViewItemClickCallback;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 生活
 */
public class PageThreeFragment extends BaseFragmentImpl implements OnRecyclerViewItemClickCallback {

    @BindView(R.id.life_recyclerview)
    RecyclerView lifeRecyclerview;
    private Unbinder unbinder;
    private LifeRecyclerViewAdapter adapter;

    @Override
    public View initView(LayoutInflater layoutInflater, ViewGroup container, Bundle saveInstanceState) {
        View view = layoutInflater.inflate(R.layout.fragment_page_three, container, false);
        unbinder = ButterKnife.bind(this,view);
        initRecyclerView();
        return view;
    }

    private void initRecyclerView() {
        adapter = new LifeRecyclerViewAdapter(getFragmentContext());
        adapter.setClickCallback(this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getFragmentContext(),2);
        lifeRecyclerview.setLayoutManager(gridLayoutManager);
        lifeRecyclerview.setAdapter(adapter);
    }

    @Override
    public void onLazyLoad() {
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onItemClick(int postion) {
        switch (postion){
            case 0:
                startActivity(new Intent(getFragmentContext(), LaugherActivity.class));
                break;
            case 1:
                break;
            case 2:
                startActivity(new Intent(getFragmentContext(), LotterActivity.class));
                break;
            case 3:
                break;
            case 4:
                break;
        }
    }
}
