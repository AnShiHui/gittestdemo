package com.ann.anlife.viewmodel;

import com.ann.anlife.base.BaseView;
import com.ann.anlife.model.LaoHuangLiInfo;

public interface BaseLaoHuangLiView extends BaseView {

   void onLaoHuangLiLoad(LaoHuangLiInfo result);

   void onLoadFail(String reason);
}
