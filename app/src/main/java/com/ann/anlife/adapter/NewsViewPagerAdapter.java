package com.ann.anlife.adapter;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * author Ann
 * description
 * createtime 2019/9/18
 */
public class NewsViewPagerAdapter extends FragmentPagerAdapter {
   private String[] titles = new String[]{"推荐","社会","国内","国际","娱乐","体育","军事","科技","财经","时尚"};
   private List<Fragment> fragmentList;
    public NewsViewPagerAdapter(@NonNull FragmentManager fm,List<Fragment> list) {
        super(fm);
        this.fragmentList = list;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return fragmentList == null ? 0 : fragmentList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
