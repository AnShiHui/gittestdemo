package com.ann.anlife.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ann.anlife.R;
import com.ann.anlife.activity.WebViewActivity;
import com.ann.anlife.adapter.NewsRecyclerViewAdapter;
import com.ann.anlife.base.BaseFragmentImpl;
import com.ann.anlife.interfaces.OnRecyclerViewItemClickCallback;
import com.ann.anlife.model.NewsInfo;
import com.ann.anlife.presenter.NewsPresenter;
import com.ann.anlife.utils.LogUtil;
import com.ann.anlife.view.ProgressConstraintLayout;
import com.ann.anlife.viewmodel.BaseNewsView;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 基础新闻fragment
 */
public class NewsFragment extends BaseFragmentImpl implements BaseNewsView, OnRecyclerViewItemClickCallback {

    @BindView(R.id.news_fragment_recyvlerview)
    RecyclerView newsFragmentRecyvlerview;
    @BindView(R.id.news_fragment_swipe)
    SwipeRefreshLayout newsFragmentSwipe;
    @BindView(R.id.news_state)
    ProgressConstraintLayout newsState;
    private String type;
    private View rootView;
    private Unbinder unbinder;
    private List<NewsInfo.ResultBean.DataBean> list = new ArrayList<>();
    private NewsRecyclerViewAdapter adapter;

    static NewsFragment getInstance(String type) {
        NewsFragment newsFragment = new NewsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        newsFragment.setArguments(bundle);
        return newsFragment;
    }

    @Override
    public View initView(LayoutInflater layoutInflater, ViewGroup container, Bundle saveInstanceState) {
        if (rootView == null) {
            rootView = layoutInflater.inflate(R.layout.fragment_news, container, false);
        }
        unbinder = ButterKnife.bind(this, rootView);
        if (getArguments() != null) {
            type = getArguments().getString("type", "toutiao");
        }
        init();
        return rootView;
    }

    private void init() {
        newsFragmentSwipe.setColorSchemeResources(R.color.red);
        adapter = new NewsRecyclerViewAdapter(list, getFragmentContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getFragmentContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        newsFragmentRecyvlerview.setLayoutManager(layoutManager);
        newsFragmentRecyvlerview.setAdapter(adapter);
        newsFragmentSwipe.setOnRefreshListener(this::onLazyLoad);
        adapter.setClickCallback(this);
        newsState.showLoading();
    }

    @Override
    public void onLazyLoad() {
        new NewsPresenter(this).getNewsInfo(type);
    }

    @Override
    public void onLoadNews(NewsInfo newsInfo) {
        LogUtil.d("loadNews");
        onStopRefresh();
        newsState.showContent();
        if (newsInfo.getResult() != null && newsInfo.getResult().getData() != null) {
            list.clear();
            list.addAll(newsInfo.getResult().getData());
            adapter.onLoadFinished(true);
        }
    }

    @Override
    public void onLoadFail(String failMsg) {
        LogUtil.d("load fail "+failMsg);
        onStopRefresh();
        newsState.showError(R.drawable.ic_error, failMsg, "", "再试一次",
                v -> new NewsPresenter(NewsFragment.this).getNewsInfo(type));
    }

    @Override
    public void onLoadEmpty() {
        onStopRefresh();
    }

    @Override
    public void onNetError() {
        onStopRefresh();
        LogUtil.d("net error");
        newsState.showNetError("网络错误，请检查您的网络",
                v -> new NewsPresenter(NewsFragment.this).getNewsInfo(type));
    }

    @Override
    public void onDestroyView() {
        if (rootView != null) {
            ((ViewGroup) rootView.getParent()).removeView(rootView);
        }
        unbinder.unbind();
        super.onDestroyView();
    }

    private void onStopRefresh() {
        if (newsFragmentSwipe != null && newsFragmentSwipe.isRefreshing()) {
            newsFragmentSwipe.setRefreshing(false);
        }
    }

    @Override
    public void onItemClick(int postion) {
        Intent intent = new Intent();
        intent.putExtra("url", list.get(postion).getUrl());
        intent.setClass(getFragmentContext(), WebViewActivity.class);
        startActivity(intent);
    }

}
