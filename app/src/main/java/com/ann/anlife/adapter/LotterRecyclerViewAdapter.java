package com.ann.anlife.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.ann.anlife.R;
import com.ann.anlife.interfaces.OnRecyclerViewItemClickCallback;
import com.ann.anlife.model.LotterSortInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * author Ann
 * description
 * createtime 2019/11/23
 */
public class LotterRecyclerViewAdapter extends RecyclerView.Adapter<LotterRecyclerViewAdapter.LotterHolder> implements View.OnClickListener {
    private Unbinder unbinder;
    private Context mContext;
    private List<LotterSortInfo.ResultBean> list;
    private OnRecyclerViewItemClickCallback itemClickCallback;

    public LotterRecyclerViewAdapter(Context mContext, List<LotterSortInfo.ResultBean> list) {
        this.mContext = mContext;
        this.list = list;
    }

    public void setItemClickCallback(OnRecyclerViewItemClickCallback itemClickCallback) {
        this.itemClickCallback = itemClickCallback;
    }

    @NonNull
    @Override
    public LotterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_lotter_sort_adapter,parent,false);
        return new LotterHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LotterHolder holder, int position) {
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(this);
        if (list.size() > 0 && list.size() > position){
            holder.tv_name.setText(list.get(position).getLottery_name());
            holder.tv_des.setText(list.get(position).getRemarks());
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        if (itemClickCallback != null){
            itemClickCallback.onItemClick(position);
        }
    }

    class LotterHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.lotter_adapter_name)
        AppCompatTextView tv_name;
        @BindView(R.id.lotter_adapter_des)
        AppCompatTextView tv_des;
        LotterHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this,itemView);
        }
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        unbinder.unbind();
    }
}
