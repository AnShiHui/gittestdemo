package com.ann.anlife.base;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ann.anlife.R;
import com.ann.anlife.interfaces.OnEachPermissionCallback;
import com.ann.anlife.interfaces.OnPermissionCallback;
import com.ann.anlife.utils.AppManager;
import com.ann.anlife.view.ProgressConstraintLayout;
import com.jaeger.library.StatusBarUtil;
import com.tbruyelle.rxpermissions2.Permission;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * author Ann
 * description
 * createtime 2019/9/18
 */
public abstract class BaseActivity extends AppCompatActivity {

    private Disposable subscribe;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppManager.getAppManager().addActivity(this);
        StatusBarUtil.setLightMode(this);
    }

    @Override
    protected void onDestroy() {
        AppManager.getAppManager().removeActivity(this);
        if (subscribe != null && !subscribe.isDisposed()){
            subscribe.dispose();
        }
        super.onDestroy();
    }

    public void switchState(int type, ProgressConstraintLayout pctl, String title, @Nullable View.OnClickListener listener){
        switch (type){
            case BaseContants.TYPE_LOADING:
                pctl.showLoading();
                break;
            case BaseContants.TYPE_EMPTY:
                pctl.showEmpty(title);
                break;
            case BaseContants.TYPE_CONTENT:
                pctl.showContent();
                break;
            case BaseContants.TYPE_ERROR:
                pctl.showError(R.drawable.ic_error,title,"","再试一次",listener);
                break;
            case BaseContants.TYPE_NET:
                pctl.showNetError(title,listener);
                break;
        }
    }

    public void showToast(String content){
        Toast.makeText(getApplicationContext(),content,Toast.LENGTH_SHORT).show();
    }

    public void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    public void checkPermissionForAll(OnPermissionCallback callback,String ...permissions){
        Observable<Boolean> request = new RxPermissions(this)
                .request(permissions);
        subscribe = request.subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) throws Exception {
                if (aBoolean) {
                    if (callback != null){
                        callback.onPermissionGanted();
                    }
                }else {
                    if (callback != null){
                        callback.onPermissionDenied();
                    }
                }
            }
        });
    }

    public void checkPermissionForEach(OnEachPermissionCallback eachPermissionCallback,String ...permissions){
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.requestEach(permissions).subscribe(new Observer<Permission>() {
            @Override
            public void onSubscribe(Disposable d) {
                subscribe = d;
            }

            @Override
            public void onNext(Permission permission) {
                    if (eachPermissionCallback != null){
                        eachPermissionCallback.onPermissionNext(permission);
                    }
            }

            @Override
            public void onError(Throwable e) {
                subscribe.dispose();
            }

            @Override
            public void onComplete() {
                subscribe.dispose();
            }
        });
    }
}
