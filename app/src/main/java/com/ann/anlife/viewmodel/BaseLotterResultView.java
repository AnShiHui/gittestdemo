package com.ann.anlife.viewmodel;

import com.ann.anlife.base.BaseView;
import com.ann.anlife.model.LotterResultInfo;

/**
 * author Ann
 * description
 * createtime 2019/11/24
 */
public interface BaseLotterResultView extends BaseView {

    void onResultLoad(LotterResultInfo info);

    void onResultFail(String reason);
}
