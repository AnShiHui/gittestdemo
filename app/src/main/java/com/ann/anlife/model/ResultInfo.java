package com.ann.anlife.model;

/**
 * author Ann
 * description
 * createtime 2019/11/23
 */
public class ResultInfo {


    /**
     * error_code : 0
     * reason : Success
     * result : {}
     */

    private int error_code;
    private String reason;

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}
