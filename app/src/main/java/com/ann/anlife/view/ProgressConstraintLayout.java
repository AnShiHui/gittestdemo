package com.ann.anlife.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ann.anlife.R;
import com.ann.anlife.interfaces.ProgressLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

public class ProgressConstraintLayout extends ConstraintLayout implements ProgressLayout {

    private final String CONTENT = "type_content";
    private final String LOADING = "type_loading";
    private final String EMPTY = "type_empty";
    private final String ERROR = "type_error";

    private LayoutInflater inflater;
    private View view;

    private List<View> contentViews = new ArrayList<>();

    private View loadingState;

    private View emptyState;
    private ImageView emptyStateImageView;
    private TextView emptyStateTitleTextView;
    private TextView emptyStateContentTextView;

    private View errorState;
    private ImageView errorStateImageView;
    private TextView errorStateTitleTextView;
    private AppCompatTextView errorStateButton;

    private String state = CONTENT;

    public ProgressConstraintLayout(Context context) {
        super(context);
    }

    public ProgressConstraintLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ProgressConstraintLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void showContent() {
        switchState(CONTENT, 0, null, null, null, null, Collections.<Integer>emptyList());
    }

    @Override
    public void showContent(List<Integer> idsOfViewsNotToShow) {
        switchState(CONTENT, 0, null, null, null, null, idsOfViewsNotToShow);
    }

    @Override
    public void showLoading() {
        switchState(LOADING, 0, null, null, null, null, Collections.<Integer>emptyList());
    }

    @Override
    public void showLoading(List<Integer> idsOfViewsNotToHide) {
        switchState(LOADING, 0, null, null, null, null, idsOfViewsNotToHide);
    }

    @Override
    public void showEmpty(String title, String description) {
        switchState(EMPTY, R.drawable.ic_nodata,title, description, null, null, Collections.<Integer>emptyList());
    }

    @Override
    public void showEmpty(Drawable icon, String title, String description) {
        switchState(EMPTY, icon, title, description, null, null, Collections.<Integer>emptyList());
    }

    @Override
    public void showEmpty(int icon, String title, String description, List<Integer> idsOfViewsNotToHide) {
        switchState(EMPTY, icon, title, description, null, null, idsOfViewsNotToHide);
    }

    @Override
    public void showEmpty(Drawable icon, String title, String description, List<Integer> idsOfViewsNotToHide) {
        switchState(EMPTY, icon, title, description, null, null, idsOfViewsNotToHide);
    }

    @Override
    public void showError(int icon, String title, String description, String buttonText, View.OnClickListener buttonClickListener) {
        switchState(ERROR, icon, title, description, buttonText, buttonClickListener, Collections.<Integer>emptyList());
    }

    @Override
    public void showError(Drawable icon, String title, String description, String buttonText, View.OnClickListener buttonClickListener) {
        switchState(ERROR, icon, title, description, buttonText, buttonClickListener, Collections.<Integer>emptyList());
    }

    @Override
    public void showError(int icon, String title, String description, String buttonText, View.OnClickListener buttonClickListener, List<Integer> idsOfViewsNotToHide) {
        switchState(ERROR, icon, title, description, buttonText, buttonClickListener, idsOfViewsNotToHide);
    }

    @Override
    public void showError(Drawable icon, String title, String description, String buttonText, View.OnClickListener buttonClickListener, List<Integer> idsOfViewsNotToHide) {
        switchState(ERROR, icon, title, description, buttonText, buttonClickListener, idsOfViewsNotToHide);
    }

    private void switchState(String state, int icon, String title, String description,
                             String buttonText, OnClickListener buttonClickListener, List<Integer> idsOfViewsNotToHide) {
        this.state = state;

        hideAllStates();

        switch (state) {
            case CONTENT:
                setContentVisibility(true, idsOfViewsNotToHide);
                break;
            case LOADING:
                setContentVisibility(false, idsOfViewsNotToHide);
                inflateLoadingView();
                break;
            case EMPTY:
                setContentVisibility(false, idsOfViewsNotToHide);
                inflateEmptyView();

                emptyStateImageView.setImageResource(icon);
                emptyStateTitleTextView.setText(title);
                break;
            case ERROR:
                setContentVisibility(false, idsOfViewsNotToHide);
                inflateErrorView();
                errorStateImageView.setImageResource(icon);
                errorStateTitleTextView.setText(title);
                errorStateButton.setText(buttonText);
                errorStateButton.setOnClickListener(buttonClickListener);
                break;
        }
    }

    private void switchState(String state, Drawable icon, String title, String description,
                             String buttonText, OnClickListener buttonClickListener, List<Integer> idsOfViewsNotToHide) {
        this.state = state;

        hideAllStates();

        switch (state) {
            case CONTENT:
                setContentVisibility(true, idsOfViewsNotToHide);
                break;
            case LOADING:
                setContentVisibility(false, idsOfViewsNotToHide);
                inflateLoadingView();
                break;
            case EMPTY:
                setContentVisibility(false, idsOfViewsNotToHide);
                inflateEmptyView();

                emptyStateImageView.setImageDrawable(icon);
                emptyStateTitleTextView.setText(title);
                emptyStateContentTextView.setText(description);
                break;
            case ERROR:
                setContentVisibility(false, idsOfViewsNotToHide);
                inflateErrorView();

                errorStateImageView.setImageDrawable(icon);
                errorStateTitleTextView.setText(title);
                errorStateButton.setText(buttonText);
                errorStateButton.setOnClickListener(buttonClickListener);
                break;
        }
    }

    private void hideAllStates() {
        hideLoadingView();
        hideEmptyView();
        hideErrorView();
    }

    private void hideLoadingView() {
        if (loadingState != null) {
            loadingState.setVisibility(GONE);
        }
    }

    private void hideEmptyView() {
        if (emptyState != null) {
            emptyState.setVisibility(GONE);
        }
    }

    private void hideErrorView() {
        if (errorState != null) {
            errorState.setVisibility(GONE);
        }
    }

    private void setContentVisibility(boolean visible, List<Integer> skipIds) {
        for (View v : contentViews) {
            if (!skipIds.contains(v.getId())) {
                v.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        }
    }

    private void inflateLoadingView() {
        if (loadingState == null) {
            view = inflater.inflate(R.layout.view_loading, null);
            loadingState = view.findViewById(R.id.layout_loading);
            loadingState.setTag(LOADING);

            LayoutParams layoutParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.topToTop = ConstraintSet.PARENT_ID;
            layoutParams.bottomToBottom = ConstraintSet.PARENT_ID;
            layoutParams.startToStart = ConstraintSet.PARENT_ID;
            layoutParams.endToEnd = ConstraintSet.PARENT_ID;

            addView(loadingState, layoutParams);
        } else {
            loadingState.setVisibility(VISIBLE);
        }
    }

    private void inflateEmptyView() {
        if (emptyState == null) {
            view = inflater.inflate(R.layout.view_empty, null);
            emptyState = view.findViewById(R.id.layout_empty);
            emptyState.setTag(EMPTY);

            emptyStateImageView = view.findViewById(R.id.image_icon);
            emptyStateTitleTextView = view.findViewById(R.id.text_title);
            emptyStateContentTextView = view.findViewById(R.id.text_description);

            LayoutParams layoutParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.topToTop = ConstraintSet.PARENT_ID;
            layoutParams.bottomToBottom = ConstraintSet.PARENT_ID;
            layoutParams.startToStart = ConstraintSet.PARENT_ID;
            layoutParams.endToEnd = ConstraintSet.PARENT_ID;

            addView(emptyState, layoutParams);
        } else {
            emptyState.setVisibility(VISIBLE);
        }
    }

    private void inflateErrorView() {
        if (errorState == null) {
            view = inflater.inflate(R.layout.view_error, null);
            errorState = view.findViewById(R.id.layout_error);
            errorState.setTag(ERROR);

            errorStateImageView = view.findViewById(R.id.net);
            errorStateTitleTextView = view.findViewById(R.id.text_title);
            errorStateButton = view.findViewById(R.id.button_retry);

            LayoutParams layoutParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.topToTop = ConstraintSet.PARENT_ID;
            layoutParams.bottomToBottom = ConstraintSet.PARENT_ID;
            layoutParams.startToStart = ConstraintSet.PARENT_ID;
            layoutParams.endToEnd = ConstraintSet.PARENT_ID;

            addView(errorState, layoutParams);
        } else {
            errorState.setVisibility(VISIBLE);
        }
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        super.addView(child, index, params);

        if (child.getTag() == null || (!child.getTag().equals(LOADING) &&
                !child.getTag().equals(EMPTY) && !child.getTag().equals(ERROR))) {

            contentViews.add(child);
        }
    }

    @Override
    public String getCurrentState() {
        return state;
    }

    @Override
    public void showNetError(String title, OnClickListener buttonClickListener) {
        switchState(ERROR,R.drawable.ic_neterror,title,"","再试一次",buttonClickListener,Collections.emptyList());
    }

    @Override
    public void showEmpty(String title) {
        switchState(EMPTY,R.drawable.ic_nodata,title,"",null,null, Collections.emptyList());
    }

    @Override
    public boolean isContentCurrentState() {
        return state.equals(CONTENT);
    }

    @Override
    public boolean isLoadingCurrentState() {
        return state.equals(LOADING);
    }

    @Override
    public boolean isEmptyCurrentState() {
        return state.equals(EMPTY);
    }

    @Override
    public boolean isErrorCurrentState() {
        return state.equals(ERROR);
    }
}
