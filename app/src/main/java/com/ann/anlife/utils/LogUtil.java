package com.ann.anlife.utils;

import android.util.Log;

import com.ann.anlife.BuildConfig;


/**
 * Created by Ann on 2019/4/10.
 * log工具.
 */
public class LogUtil {

    private static final String TAG = "AnLife";

    private static boolean isDebug = BuildConfig.LOG_DEBUG;


    public static void setIsDebug(boolean isDebug) {
        LogUtil.isDebug = isDebug;
    }

    public static void d(String message) {
        if (isDebug){
            Log.d(TAG, message);
        }
    }

    public static void i(String message) {
        if (isDebug){
        Log.i(TAG, message);
        }
    }

    public static void e(String message, Throwable throwable) {
        if (isDebug){
            Log.e(TAG, message, throwable);
        }
    }
}
