package com.ann.anlife.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ann.anlife.R;
import com.ann.anlife.interfaces.OnRecyclerViewItemClickCallback;
import com.ann.anlife.model.NewsInfo;
import com.ann.anlife.view.CircleImageView;
import com.bumptech.glide.Glide;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * author Ann
 * description
 * createtime 2019/9/18
 */
public class NewsRecyclerViewAdapter extends RecyclerView.Adapter implements View.OnClickListener {

    private List<NewsInfo.ResultBean.DataBean> list;
    private Context context;
    private static final int TYPE_ONE = 0;
    private static final int TYPE_TWO = 1;
    private static final int TYPE_THREE = 2;
    private static final int TYPE_NONE = 3;
    private static final int TYPE_FOOTER = 4;
    private OnRecyclerViewItemClickCallback clickCallback;
    private boolean isSuccess;

    public NewsRecyclerViewAdapter(List<NewsInfo.ResultBean.DataBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void setClickCallback(OnRecyclerViewItemClickCallback clickCallback) {
        this.clickCallback = clickCallback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
       if (viewType == TYPE_NONE){
           View withoutView = inflater.inflate(R.layout.layout_newsitem_none,parent,false);
           return new WithoutPicHolder(withoutView);
       }else if (viewType == TYPE_ONE || viewType == TYPE_TWO){
           View onePicView = inflater.inflate(R.layout.layout_newsitem_one,parent,false);
           return new OnePicHolder(onePicView);
       }else if (viewType == TYPE_THREE){
           View threeView = inflater.inflate(R.layout.layout_newsitem_three,parent,false);
           return new ThreePicHolder(threeView);
       }else if (viewType == TYPE_FOOTER){
           View footerView = inflater.inflate(R.layout.layout_newsitem_foot,parent,false);
           return new FooterHolder(footerView);
       }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof WithoutPicHolder){
            WithoutPicHolder withoutPicHolder = (WithoutPicHolder) holder;
            withoutPicHolder.tv_info.setText(list.get(position).getAuthor_name()+"\n"+list.get(position).getDate());
            withoutPicHolder.tv_title.setText(list.get(position).getTitle());
            withoutPicHolder.itemView.setTag(position);
            withoutPicHolder.itemView.setOnClickListener(this);
        }else if (holder instanceof OnePicHolder){
            OnePicHolder onePicHolder = (OnePicHolder) holder;
            onePicHolder.tv_info.setText(list.get(position).getAuthor_name()+"\n"+list.get(position).getDate());
            onePicHolder.tv_title.setText(list.get(position).getTitle());
            Glide.with(context).asBitmap().load(list.get(position).getThumbnail_pic_s()).into(onePicHolder.circleImageView);
            onePicHolder.itemView.setTag(position);
            onePicHolder.itemView.setOnClickListener(this);
        }else if (holder instanceof ThreePicHolder){
            ThreePicHolder threePicHolder = (ThreePicHolder) holder;
            threePicHolder.tv_info.setText(list.get(position).getAuthor_name()+"\n"+list.get(position).getDate());
            threePicHolder.tv_title.setText(list.get(position).getTitle());
            threePicHolder.itemView.setTag(position);
            threePicHolder.itemView.setOnClickListener(this);
            Glide.with(context).asBitmap().load(list.get(position).getThumbnail_pic_s()).into(threePicHolder.iv_one);
            Glide.with(context).asBitmap().load(list.get(position).getThumbnail_pic_s02()).into(threePicHolder.iv_two);
            Glide.with(context).asBitmap().load(list.get(position).getThumbnail_pic_s03()).into(threePicHolder.iv_three);
        }else if (holder instanceof FooterHolder){
            FooterHolder footerHolder = (FooterHolder) holder;
            if (isSuccess){
                footerHolder.itemView.setVisibility(View.GONE);
            }else {
                footerHolder.contentLoadingProgressBar.setVisibility(View.GONE);
                footerHolder.textView.setText("没有更多数据");
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (getItemCount() > 0 ){
            if (position < list.size()){
                if (list.get(position).getThumbnail_pic_s03() == null && list.get(position).getThumbnail_pic_s02() != null){
                    return TYPE_TWO;
                }else if (list.get(position).getThumbnail_pic_s02() == null && list.get(position).getThumbnail_pic_s() != null){
                    return TYPE_ONE;
                }else if (list.get(position).getThumbnail_pic_s03() != null){
                    return TYPE_THREE;
                }else if (list.get(position).getThumbnail_pic_s() == null){
                    return TYPE_NONE;
                }
            }else {
                return TYPE_FOOTER;
            }
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public void onLoadFinished(boolean isSuccess){
       this.isSuccess = isSuccess;
       notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
        if (clickCallback != null){
            clickCallback.onItemClick(position);
        }
    }

    class ThreePicHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.news_itemthree_info)
        AppCompatTextView tv_info;
        @BindView(R.id.news_itemthree_title)
        AppCompatTextView tv_title;
        @BindView(R.id.news_itemthree_one)
        AppCompatImageView iv_one;
        @BindView(R.id.news_itemthree_two)
        AppCompatImageView iv_two;
        @BindView(R.id.news_itemthree_three)
        AppCompatImageView iv_three;
        ThreePicHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    class OnePicHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.news_itemone_info)
         AppCompatTextView tv_info;
        @BindView(R.id.news_itemone_title)
        AppCompatTextView tv_title;
        @BindView(R.id.news_itemone_icon)
        CircleImageView circleImageView;
        OnePicHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    class WithoutPicHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.news_itemnone_info)
         AppCompatTextView tv_info;
        @BindView(R.id.news_itemnone_title)
        AppCompatTextView tv_title;
        WithoutPicHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    class FooterHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.footer_progress)
        ContentLoadingProgressBar contentLoadingProgressBar;
        @BindView(R.id.footer_title)
        AppCompatTextView textView;
        FooterHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
