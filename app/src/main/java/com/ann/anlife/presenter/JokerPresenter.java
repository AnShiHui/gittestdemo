package com.ann.anlife.presenter;

import com.alibaba.fastjson.JSON;
import com.ann.anlife.base.BaseImpl;
import com.ann.anlife.http.HttpImpl;
import com.ann.anlife.http.HttpUrls;
import com.ann.anlife.interfaces.OnHttpCallback;
import com.ann.anlife.model.JokerInfo;
import com.ann.anlife.viewmodel.BaseJokerView;

import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;

/**
 * author Ann
 * description
 * createtime 2019/11/24
 */
public class JokerPresenter extends BaseImpl {

    private HttpImpl http;
    private BaseJokerView jokerView;
    private CompositeDisposable compositeDisposable = getCompositeDisposabe();

    public JokerPresenter(BaseJokerView jokerView) {
        this.jokerView = jokerView;
        if (http == null){
            http = new HttpImpl(compositeDisposable);
        }
    }

    public void getJoker(int page,int pagesize){
        Map<String,Object> map = getHashMap();
        map.put("key","da25bca36d3d7df4034603f522ac1248");
        map.put("page",page);
        map.put("pagesize",pagesize);
        http.startRequest(HttpUrls.mLaughUpdatedUrl, map, new OnHttpCallback() {
            @Override
            public void onSuccess(String result) {
                jokerView.onJokerLoad(JSON.parseObject(result,JokerInfo.class));
            }

            @Override
            public void onFail(String reason) {
                jokerView.onJokerFail(reason);
            }

            @Override
            public void onNetError(String error) {
                jokerView.onNetError();
            }
        });
    }

    @Override
    public void onRelease() {
        if (compositeDisposable != null){
            compositeDisposable.clear();
        }
    }
}
