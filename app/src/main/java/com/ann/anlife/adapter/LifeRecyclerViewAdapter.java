package com.ann.anlife.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ann.anlife.R;
import com.ann.anlife.interfaces.OnRecyclerViewItemClickCallback;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * author Ann
 * description 生活recyclerview适配器
 * createtime 2019/9/30
 */
public class LifeRecyclerViewAdapter extends RecyclerView.Adapter implements View.OnClickListener {

    private Context mContext;
    private OnRecyclerViewItemClickCallback clickCallback;

    public LifeRecyclerViewAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setClickCallback(OnRecyclerViewItemClickCallback clickCallback) {
        this.clickCallback = clickCallback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_recycler_life, parent, false);
        return new LifeHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        LifeHolder lifeHolder = (LifeHolder) holder;
        lifeHolder.itemView.setTag(position);
        lifeHolder.itemView.setOnClickListener(this);
        switch (position){
            case 0:
                lifeHolder.lifeAdapterIcon.setImageResource(R.drawable.ic_laugher);
                lifeHolder.lifeAdapterTitle.setText("笑话大全");
                break;
            case 1:
                lifeHolder.lifeAdapterIcon.setImageResource(R.drawable.ic_stars);
                lifeHolder.lifeAdapterTitle.setText("星座运势");
                break;
            case 2:
                lifeHolder.lifeAdapterIcon.setImageResource(R.drawable.ic_caipiao);
                lifeHolder.lifeAdapterTitle.setText("彩票开奖");
                break;
            case 3:
                lifeHolder.lifeAdapterIcon.setImageResource(R.drawable.ic_phoneno);
                lifeHolder.lifeAdapterTitle.setText("手机归属");
                break;
            case 4:
                lifeHolder.lifeAdapterIcon.setImageResource(R.drawable.ic_qq);
                lifeHolder.lifeAdapterTitle.setText("QQ号吉凶");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
        if (clickCallback != null){
            clickCallback.onItemClick(position);
        }
    }

    class LifeHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.life_adapter_icon)
        AppCompatImageView lifeAdapterIcon;
        @BindView(R.id.life_adapter_title)
        AppCompatTextView lifeAdapterTitle;
        @BindView(R.id.life_adapter_root)
        LinearLayout lifeAdapterRoot;
        LifeHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
