package com.ann.anlife.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.ann.anlife.R

class SettingAdapter(private val context: Context,private val mList:ArrayList<String>) : RecyclerView.Adapter<SettingAdapter.SettingHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingHolder {
       return SettingHolder(LayoutInflater.from(context).inflate(R.layout.layout_adapter_setting,parent,false))
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: SettingHolder, position: Int) {
        holder.mTitle.text = mList[position]
    }

    class SettingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mTitle = itemView.findViewById<AppCompatTextView>(R.id.adapter_setting_title)
    }
}